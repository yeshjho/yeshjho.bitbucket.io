function updateCell(x, y, bCellExists, force=false) {
  index = getBoundedIndex(x, y);
  if (getCell(...index) == bCellExists && !force)
    return;
  
  x = index[0];
  y = index[1];
  
  let coord = getCoordByIndex(x, y);
  push();
  noStroke();
  fill(...getColor(bCellExists));
  rect(coord[0] + 1.5, coord[1] + 1.5, RECT_WIDTH - 1.5, RECT_HEIGHT - 1.5);
  pop();
  
  cells[x][y] = bCellExists;
}


function updateEntireCells() {
  class CellToBeUpdated {
    constructor(x, y, state) {
      this.x = x;
      this.y = y;
      this.state = state;
    }
  }
  cellsToBeUpdated = [];
  
  for (let row = 0; row < ROW; ++row) {
    for (let column = 0; column < COLUMN; ++column) {
      neighborLiveCellCount = getCell(column - 1, row - 1) + getCell(column, row - 1) +
                              getCell(column + 1, row - 1) + getCell(column - 1, row) +
                              getCell(column + 1, row) + getCell(column - 1, row + 1) +
                              getCell(column, row + 1) + getCell(column + 1, row + 1);
      if (cells[column][row]) {
        if (neighborLiveCellCount < 2 || neighborLiveCellCount > 3) {
          cellsToBeUpdated.push(new CellToBeUpdated(column, row, false));
        }
      }
      else {
        if (neighborLiveCellCount == 3) {
          cellsToBeUpdated.push(new CellToBeUpdated(column, row, true));
        }
      }
    }
  }
  
  cellsToBeUpdated.forEach(cell => updateCell(cell.x, cell.y, cell.state));
}


function randomlyUpdateEntireCells() {
  for (let row = 0; row < ROW; ++row) {
    for (let column = 0; column < COLUMN; ++column) {
      updateCell(column, row, random() < 0.5);
    }
  }
}


function spawnPattern(x, y, pattern) {
  let coords;

  switch (pattern) {
    case EPattern.Pulsar:
      coords = [...pattern,
                ...inverse(true, false, pattern),
                ...inverse(false, true, pattern),
                ...inverse(true, true, pattern)];
      break;
    default:
      coords = pattern;
      break;
  }
  
  for (let coord of coords) {
    updateCell(x + coord[0], y + coord[1], true);
  }
}