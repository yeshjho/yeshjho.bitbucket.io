function drawGrid() {
  background(...getColor(false));
  
  push();
  stroke(...CELL_COLOR);
  for (let x = 0; x <= width; x += RECT_WIDTH) {
    line(x, 0, x, height);
  }
  for (let y = 0; y <= height; y += RECT_HEIGHT) {
    line(0, y, width, y);
  }
  pop();
}


function drawInitialGrid() {
  RECT_WIDTH = width / COLUMN;
  RECT_HEIGHT = height / ROW;
  
  cells = [];
  // prepare cells array
  // cells are on a sphere
  for (let column = 0; column < COLUMN; ++column) {
    cells.push([]);
    for (let row = 0; row < ROW; ++row) {
      cells[column].push(false);
    }
  }

  drawGrid();
  randomlyUpdateEntireCells();
}