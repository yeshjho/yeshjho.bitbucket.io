// Written by Joonho Hwang
// Game of Life Assignment
// CS099
// Spring 2019


function setup() {
  createCanvas(windowWidth, windowHeight);
  
  alert("Press E to enable Edit mode." +
        "\nPress F to change Frame rate." +
        "\nPress Z to change canvas siZe." +
        "\nPress L to change cell coLors.");
  
  drawInitialGrid();
  
  resume();
}

function draw() {
}

function keyPressed() {
  switch (keyCode) {
    case 82:  // r for Random
      randomlyUpdateEntireCells();
      break;
    case 67:  // c for Clear
      for (let row = 0; row < ROW; ++row) {
        for (let column = 0; column < COLUMN; ++column) {
          updateCell(column, row, false);
        }
      }
      drawGrid();
      break;
    case 32:  // space for requirement
      spawnPattern(floor(random(0, COLUMN)), floor(random(0, ROW)), 
                   random(P_STILL));
      break;
    case 69:  // e for Edit
      if (bEditing) {
        resume();
        bEditing = false;
      }
      else {
        pause();
        bEditing = true;
      }
      break;
    case 70:  // f for Frame
      pause();
      fps = map(int(prompt("Enter fps(1-60):", fps)),
                MIN_FPS, MAX_FPS, MIN_FPS, MAX_FPS, true);
      resume();
      break;
    case 90:  // z for siZe
      bEditing = false;
      
      pause();
      COLUMN = int(prompt("Enter Column:", COLUMN));
      if (COLUMN < 1)
        COLUMN = 1;
      ROW = int(prompt("Enter Row:", ROW));
      if (ROW < 1)
        ROW = 1;
      drawInitialGrid();
      resume();
      break;
    case 76:  // l for coLor
      pause();
      
      let alive_cell = prompt("Enter Alive Cells' RGB(seperate with comma):",
                              CELL_COLOR[0] + "," + CELL_COLOR[1] + "," + CELL_COLOR[2])
                       .split(",");
      let pACC = CELL_COLOR;
      CELL_COLOR = [...alive_cell];
      
      let dead_cell = prompt("Enter Dead Cells' RGB(seperate with comma):",
                             DEAD_CELL_COLOR[0] + "," +
                             DEAD_CELL_COLOR[1] + "," + DEAD_CELL_COLOR[2])
                      .split(",");
      let pDCC = DEAD_CELL_COLOR;
      DEAD_CELL_COLOR = [...dead_cell];
      
      drawGrid();
      for (let row = 0; row < ROW; ++row) {
        for (let column = 0; column < COLUMN; ++column) {
          updateCell(column, row, getCell(column, row), true);
        }
      }
      resume();
      break;
  }
}

function mousePressed() {
  let coord = getIndexByCoord(mouseX, mouseY);
  switch (mouseButton) {
    case LEFT:
      if (bEditing) {
        updateCell(...coord, true);
      }
      else {
        spawnPattern(...coord, random(P_OSCILLATOR));
      }
      break;
    case CENTER:
      if (bEditing) {
        updateCell(...coord, false);
      }
      else {
        spawnPattern(...coord, random(P_SPACESHIP));
      }
      break;
  }
}

function mouseDragged() {
  let coord = getIndexByCoord(mouseX, mouseY);
  switch (mouseButton) {
    case LEFT:
      if (bEditing) {
        updateCell(...coord, true);
      }
      break;
    case CENTER:
      if (bEditing) {
        updateCell(...coord, false);
      }
      break;
  }
}