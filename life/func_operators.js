function pause() {
  if (updater)
    clearInterval(updater);
    updater = null;
}


function resume() {
  if (updater)
    return;
  updater = setInterval(() => updateEntireCells(), 1000 / fps);
}