function getColor(bCellExists) {
  if (bCellExists)
    return CELL_COLOR;
  else
    return DEAD_CELL_COLOR;
}


function getCoordByIndex(x, y) {
  return [x * RECT_WIDTH, y * RECT_HEIGHT];
}


function getIndexByCoord(x, y) {
  return [floor(x / RECT_WIDTH), floor(y / RECT_HEIGHT)]; 
}


function getBoundedIndex(x, y) {
  if (x < 0)
    x += COLUMN;
  else if (x > COLUMN - 1)
    x -= COLUMN;
  
  if (y < 0)
    y += ROW;
  else if (y > ROW - 1)
    y -= ROW;
  
  return [x, y];
}


function getCell(x, y) {
  index = getBoundedIndex(x, y);
  
  return cells[index[0]][index[1]];
}


function inverse(bInverseX, bInverseY, coordsToInverse) {
  to_return = [];
  for (let coord of coordsToInverse) {
    let x = coord[0];
    let y = coord[1];
    if (bInverseX)
      x *= -1;
    if (bInverseY)
      y *= -1;
    to_return.push([x, y]);
  }
  return to_return;
}