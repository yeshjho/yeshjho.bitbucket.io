let COLUMN = 100, ROW = 100;
let RECT_WIDTH, RECT_HEIGHT;
const MIN_FPS = 1, MAX_FPS = 60;
let fps = 60;
let CELL_COLOR = [150, 150, 200], DEAD_CELL_COLOR = [255, 255, 255];

const EPattern = {
  Block: [[0, 0], [0, 1], [1, 0], [1, 1]],
  Beehive: [[0, 1], [1, 0], [1, 2], [2, 0], [2, 2], [3, 1]],
  Loaf: [[0, 1], [1, 0], [1, 2], [2, 0], [2, 3], [3, 1], [3, 2]],
  Boat: [[-1, -1], [-1, 0], [0, -1], [0, 1], [1, 0]],
  Tub: [[-1, 0], [0, -1], [0, 1], [1, 0]],
  
  Blinker: [[-1, 0], [0, 0], [1, 0]],
  Toad: [[0, 1], [1, 0], [1, 1], [2, 0], [2, 1], [3, 0]],
  Beacon: [[0, 0], [0, 1], [1, 0], [2, 3], [3, 2], [3, 3]],
  Pulsar: [[1, 2], [1, 3], [1, 4], [2, 1], [2, 6], [3, 1], [3, 6],
           [4, 1], [4, 6], [6, 2], [6, 3], [6, 4]],
  Penta: [[0, 2], [0, 7], [1, 0], [1, 1], [1, 3], [1, 4],
                [1, 5], [1, 6], [1, 8], [1, 9], [2, 2], [2, 7]],
  
  Glider: [[-1, 1], [0, -1], [0, 1], [1, 0], [1, 1]],
  L_Spaceship: [[0, 0], [0, 2], [1, 3], [2, 3], [3, 0], [3, 3], [4, 1], [4, 2], [4, 3]],
  M_Spaceship: [[0, 2], [0, 3], [0, 4], [1, 1], [1, 4], [2, 4],
                [3, 0], [3, 4], [4, 4], [5, 1], [5, 3]],
  H_Spaceship: [[0, 2], [0, 3], [0, 4], [1, 1], [1, 4], [2, 4], [3, 0], [3, 4],
                [4, 0], [4, 4], [5, 4], [6, 1], [6, 3]]
};
const P_STILL = [EPattern.Block, EPattern.Beehive, EPattern.Loaf,
                 EPattern.Boat, EPattern.Tub];
const P_OSCILLATOR = [EPattern.Blinker, EPattern.Toad,
                      EPattern.Beacon, EPattern.Pulsar, EPattern.Penta];
const P_SPACESHIP = [EPattern.Glider, EPattern.L_Spaceship,
                     EPattern.M_Spaceship, EPattern.H_Spaceship];

let cells = [];
let updater;
let bEditing = false;