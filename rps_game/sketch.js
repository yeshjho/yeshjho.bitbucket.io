// Written by Joonho Hwang
// Rock Paper Scissors Assignment
// CS099
// Spring 2019


const EShape = {Rock: 0, Paper: 1, Scissors: 2};
const EWinnerState = {p1: 0, p2: 1, tie: 2};

let p1 = {shape: null};
let p2 = {shape: null};

let images = {rock: null, paper: null, scissors: null};
let sounds = {p1Win: null, p2Win: null, tie: null};

let bShowedResult = false;

function preload() {
  images.rock = loadImage('rock.png');
  images.paper = loadImage('paper.png');
  images.scissors = loadImage('scissors.png');
  
  sounds.p1Win = loadSound('p1Win.mp3');
  sounds.p2Win = loadSound('p2Win.mp3');
  sounds.tie = loadSound('tie.mp3');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  
  textAlign(CENTER, CENTER);
  textSize(20);
  imageMode(CENTER);
  
  resetResult();
}

function draw() {
  
}

function keyPressed() {
  switch(keyCode) {
    case 65: //a
      setShape(EShape.Rock, p1);
      break;
    case 83: //s
      setShape(EShape.Paper, p1);
      break;
    case 68: //d
      setShape(EShape.Scissors, p1);
      break;
    case 72: //h
      setShape(EShape.Rock, p2);
      break;
    case 74: //j
      setShape(EShape.Paper, p2);
      break;
    case 75: //k
      setShape(EShape.Scissors, p2);
      break;
  }
  if(isBothDecided(p1, p2)) {
    if(bShowedResult) {
      bShowedResult = false;
	  resetResult();
    }
    else {
      bShowedResult = true;
      showResult(getWinner(p1.shape, p2.shape));
    }
  }
}

function isBothDecided(player1, player2) {
  return player1.shape != null && player2.shape != null;
}

function setShape(shape, player) {
  player.shape = shape;
}

function getWinner(p1State, p2State) {
  switch(p1State - p2State) {
    case 0:
      return EWinnerState.tie;
    case 1:
    case -2:
      return EWinnerState.p1;
    default:
      return EWinnerState.p2;
  }
}

function resetResult() {
  background(255);
  fill(0);
  text("Rock: A / H\nPaper: S / J\nScissors: D / K", width/2, height/2);
  p1.shape = null;
  p2.shape = null;
}

function showResult(winnerState) {
  background(255);
  
  let left = [width/4, height/4];
  let right = [width * 3/4, height/4];
  let middle = [width/2, height/4];
  
  switch(winnerState) {
    case EWinnerState.p1:
      fill(0, 0, 255);
      text("Win!", ...left);
      fill(255, 0, 0);
      text("Lose!", ...right);
      fill(200, 200, 0);
      text("Press any key to restart", ...middle);
      break;
    case EWinnerState.p2:
      fill(255, 0, 0);
      text("Lose!", ...left);
      fill(0, 0, 255);
      text("Win!", ...right);
      fill(200, 200, 0);
      text("Press any key to restart", ...middle);
      break;
    case EWinnerState.tie:
      fill(200, 200, 0);
      text("Tie!\nPress any key to restart", ...middle);
      break;
  }
  drawShape(p1.shape, true);
  drawShape(p2.shape, false);
  playSound(winnerState);
}

function drawShape(shape, isLeft) {
  let x = isLeft ? width/4 : width * 3/4;
  let y = height/2;
  let size = [100, 100];
  switch(shape) {
    case EShape.Rock:
      image(images.rock, x, y, ...size);
      break;
    case EShape.Paper:
      image(images.paper, x, y, ...size);
      break;
    case EShape.Scissors:
      image(images.scissors, x, y, ...size);
      break;
  }
}

function playSound(winner) {
  switch(winner) {
    case EWinnerState.p1:
      sounds.p1Win.play();
      break;
    case EWinnerState.p2:
      sounds.p2Win.play();
      break;
    case EWinnerState.tie:
      sounds.tie.play();
      break;
  }
}