// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class BoardCell {
  constructor(boardGame, id, x, y, w, h, type, bIsHidden, bForceTrigger, extraValue) {
    this.boardGame = boardGame;
    this.id = id;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    switch (this.id) {
      case 0:
        this.type = ECellType.StartingPoint;
        break;
      case 29:
      case 39:
        this.type = ECellType.CoinPlus;
        this.bIsHidden = false;
        break;
      default:
        this.type = type;
        this.bIsHidden = bIsHidden;
        break;
    }
    this.bForceTrigger = bForceTrigger;
    
    this.onPlayers = [];
    
    this.onTriggerEnd = null;
    
    switch (this.type) {
      case ECellType.MoveForward:
      case ECellType.MoveBackward:
        this.moveAmount = extraValue;
        break;
      case ECellType.Warp:
        this.warpDestination = extraValue;
        break;
      case ECellType.Seperator:
        this.pathChoices = extraValue;
        break;
      case ECellType.Diamond:
        for (let player of this.boardGame.players) {  // for resetRandomly
          if (player.bHasDiamond) {
            this.bHasDiamond = false;
            break;
          }
        }
        this.bHasDiamond = this.bHasDiamond == undefined ? true : false;
        break;
    }
  }
  
  trigger(player) {
    let targetPlayers;
    let playerNames;
    if (this.bIsHidden)
      this.bIsHidden = false;
    switch (this.type) {
      case ECellType.StartingPoint:
        if (player.bHasDiamond) {
          setTimeout(() => this.boardGame.gameEnd(player), 700);
        }
        else {
          player.coin += 5;
          
          if (this.onTriggerEnd) {
            setTimeout(() => {
              this.onTriggerEnd();
              this.onTriggerEnd = null;
            }, 700);
          }
        }
        break;
      case ECellType.CoinPlus:
        sfx.coin_up.play();
        player.coin += 3;
        
        if (this.onTriggerEnd) {
          setTimeout(() => {
            this.onTriggerEnd();
            this.onTriggerEnd = null;
          }, 700);
        }
        break;
      case ECellType.CoinMinus:
        sfx.coin_down.play();
        player.coin -= 3;
        
        if (this.onTriggerEnd) {
          setTimeout(() => {
            this.onTriggerEnd();
            this.onTriggerEnd = null;
          }, 700);
        }
        break;
      case ECellType.MoveForward:
        player.move(this.moveAmount, false);
        break;
      case ECellType.MoveBackward:
        player.move(-this.moveAmount, false);
        break;
      case ECellType.UFO:
        targetPlayers = [...this.boardGame.players];
        playerNames = [];
        targetPlayers.splice(targetPlayers.indexOf(player), 1);
        targetPlayers.forEach(otherPlayer => playerNames.push(otherPlayer.name));
        
        dice.onRollEnd = () => {
          for (let otherPlayer of boardGame.players) {
            if (otherPlayer.name == dice.result) {
              sfx.warp.play();
              [player.cellOn, otherPlayer.cellOn] = [otherPlayer.cellOn, player.cellOn];
              [player.beforeHellWarpPosition, otherPlayer.beforeHellWarpPosition] = 
                [otherPlayer.beforeHellWarpPosition, player.beforeHellWarpPosition];
              break;
            }
          }
          
          if (this.onTriggerEnd) {
            setTimeout(() => {
              this.onTriggerEnd();
              this.onTriggerEnd = null;
            }, 700);
          }
        };
        dice.roll(player, playerNames);
        break;
      case ECellType.Warp:
        if (this.warpDestination == 29) {
          player.beforeHellWarpPosition = this.id;
        }
        
        if (this.id == 38) {
          player.cellOn = player.beforeHellWarpPosition;
        }
        else {
          player.cellOn = this.warpDestination;
        }
        
        if (this.onTriggerEnd) {
          setTimeout(() => {
            this.onTriggerEnd();
            this.onTriggerEnd = null;
          }, 700);
        }
        
        sfx.warp.play();
        break;
      case ECellType.ResetRandomly:
        this.boardGame.cells = [];
        this.boardGame.generateCells();
        for (let player of this.boardGame.players) {
          this.boardGame.getCellById(player.cellOn).onPlayers.push(player);
        }
        
        if (this.onTriggerEnd) {
          setTimeout(() => {
            this.onTriggerEnd();
            this.onTriggerEnd = null;
          }, 700);
        }
        break;
      case ECellType.OneOnOneFight:
        targetPlayers = [...this.boardGame.players];
        playerNames = [];
        targetPlayers.splice(targetPlayers.indexOf(player), 1);
        targetPlayers.forEach(otherPlayer => playerNames.push(otherPlayer.name));
        
        dice.onRollEnd = () => {
          let otherPlayer;
          for (let player_ of this.boardGame.players) {
            if (player_.name == dice.result) {
              otherPlayer = player_;
              break;
            }
          }
          
          setTimeout(() => {
            this.boardGame.miniGame =
              new MiniGame(this.boardGame, [player, otherPlayer],
                           EMiniGameType.Individual, undefined, false);
            this.boardGame.state = EGameState.MiniGame;
          }, 700);
        };
        dice.roll(player, playerNames);
        break;
      case ECellType.TwoOnTwoFight:
        targetPlayers = [...this.boardGame.players];
        playerNames = [];
        targetPlayers.splice(targetPlayers.indexOf(player), 1);
        targetPlayers.forEach(otherPlayer => playerNames.push(otherPlayer.name));
        
        dice.onRollEnd = () => {
          let otherPlayer;
          let otherTeam = [];
          for (let player_ of this.boardGame.players) {
            if (player_.name == dice.result)
              otherPlayer = player_;
            else
              otherTeam.push(player_);
          }
          otherTeam.splice(otherTeam.indexOf(player), 1);
          
          setTimeout(() => {
            this.boardGame.miniGame =
              new MiniGame(this.boardGame, [[player, otherPlayer], [...otherTeam]],
                           EMiniGameType.TwoOnTwo, undefined, false);
            this.boardGame.state = EGameState.MiniGame;
          }, 700);
        };
        dice.roll(player, playerNames);
        break;
      case ECellType.OneOnThreeFight:
        let otherTeam = [...this.boardGame.players];
        otherTeam.splice(otherTeam.indexOf(player), 1);

        setTimeout(() => {
          this.boardGame.miniGame =
            new MiniGame(this.boardGame, [player, [...otherTeam]],
                         EMiniGameType.OneOnThree, undefined, false);
          this.boardGame.state = EGameState.MiniGame;
        }, 700);
        break;
      case ECellType.Diamond:
        if (this.bHasDiamond) {
          ui.onUIEnd = () => {
            if (this.onTriggerEnd) {
              setTimeout(() => {
                this.onTriggerEnd();
                this.onTriggerEnd = null;
              }, 700);
            }
          };
          ui.open([[EItemType.Diamond, EItemCost[EItemType.Diamond]]], player);
        }
        else {
          if (this.onTriggerEnd) {
            this.onTriggerEnd();
            this.onTriggerEnd = null;
          }
        }
        break;
      case ECellType.Shop:
        let items = [];
        for (let i = 0; i < random(2, 5); ++i) {
          let randomItem = random([
            EItemType.Thief,
            EItemType.Add3, EItemType.Add3, EItemType.Add3, EItemType.Add3,
            EItemType.Minus3, EItemType.Minus3, EItemType.Minus3, EItemType.Minus3,
            EItemType.Mul2, EItemType.Mul2,
            EItemType.OddOnly, EItemType.OddOnly, EItemType.OddOnly, 
            EItemType.EvenOnly, EItemType.EvenOnly, EItemType.EvenOnly
          ])
          items.push([randomItem, EItemCost[randomItem]]);
        }
        ui.onUIEnd = () => {
          if (this.onTriggerEnd) {
            setTimeout(() => {
              this.onTriggerEnd();
              this.onTriggerEnd = null;
            }, 700);
          }
        };
        ui.open(items, player);
        break;
      case ECellType.Seperator: // BUG: Next cell is not triggered if players stops here
        let path = {};
        let uiItems;
        switch (this.id) {  // Ugly hard-coding. Need to replace it with nickname system
          case 11:
            uiItems = [[EItemType.Left, 0], [EItemType.Right, 10]];
            path.LEFT = 25;
            path.RIGHT = 12;
            break;
          case 45:
            uiItems = [[EItemType.Down, 0], [EItemType.Right, 10]];
            path.DOWN = 46;
            path.RIGHT = 55;
            break;
          case 68:
            uiItems = [[EItemType.Left, 0], [EItemType.Down, 10]];
            path.LEFT = 69;
            path.DOWN = 72;
            break;
          case 85:
            uiItems = [[EItemType.Up, 0], [EItemType.Left, 10]];
            path.UP = 86;
            path.LEFT = 89;
            break;
          case 102:
            uiItems = [[EItemType.Right, 0], [EItemType.Up, 10]];
            path.RIGHT = 103;
            path.UP = 106;
            break;
        }
        
        ui.onUIEndArguments = [path];
        ui.onUIEnd = (path, direction) => {
          player.cellOn = path[direction];
          if (this.onTriggerEnd) {
            setTimeout(() => {
              this.onTriggerEnd();
              this.onTriggerEnd = null;
            }, 700);
          }
        };
        ui.open(uiItems, player, true);
        break;
    }
  }
  
  draw() {
    let coord = this.boardGame.getCoordAccordingCamera(this.x, this.y);
    
    // Rectangle
    push();
    if (this.bIsHidden) {
      fill(230, 230, 0);
    }
    else {
      fill(...ECellColor[this.type]);
    }
    strokeWeight(2);
    rectMode(CENTER);
    rect(coord.x, coord.y, this.w, this.h, 15);
    pop();
    
    // Image
    push();
    imageMode(CENTER);
    textAlign(CENTER, CENTER);
    if (this.bIsHidden) {
      image(img.cell_hidden, coord.x, coord.y, this.w, this.h);
    }
    else {
      switch (this.type) {
        case ECellType.CoinPlus:
        case ECellType.CoinMius:
          break;
        case ECellType.MoveForward:
        case ECellType.MoveBackward:
          fill(0);
          stroke(255);
          strokeWeight(3);
          textSize(20);
          text((this.type == ECellType.MoveForward ? "+" : "-") + this.moveAmount,
               coord.x, coord.y);
          break;
        case ECellType.Warp:
          switch (this.warpDestination) {
            case 29:  // Hell
              image(img.warp_hell, coord.x, coord.y, this.w, this.h);
              break;
            case 39:  // Heaven
              image(img.warp_heaven, coord.x, coord.y, this.w, this.h);
              break;
            case 13:  // Overworld
            case 18:
              image(img.warp_overworld, coord.x, coord.y, this.w, this.h);
              break;
            case 4:
            case 26:
              image(img.warp_1, coord.x, coord.y, this.w, this.h);
              break;
            case 43:
            case 87:
              image(img.warp_2, coord.x, coord.y, this.w, this.h);
              break;
            case 58:
            case 92:
              image(img.warp_3, coord.x, coord.y, this.w, this.h);
              break;
            case 62:
            case 96:
              image(img.warp_4, coord.x, coord.y, this.w, this.h);
              break;
          }
          break;
        case ECellType.Diamond:
          if (this.bHasDiamond) {
            image(img.shining_diamond, coord.x, coord.y, this.w, this.h);
          }
          break;
        default:
          let cellImage = img["cell_" + this.type];
          if (cellImage)
            image(cellImage, coord.x, coord.y, this.w, this.h);
          break;
      }
    }
    // fill(0);
    // stroke(255);
    // strokeWeight(3);
    // textSize(20);
    // text(this.id, coord.x, coord.y);
    pop();
    
    
    let offset, size;
    switch (this.onPlayers.length) {
      case 1:
        offset = [[0, 0]];
        size = 17;
        break;
      case 2:
        offset = [[-9, 9], [9, -9]];
        size = 12;
        break;
      case 3:
        offset = [[-11, 10], [0, -10], [11, 10]];
        size = 11;
        break;
      case 4:
        offset = [[-10, 10], [-10, -10], [10, -10], [10, 10]];
        size = 10;
        break;
    }
    // Players
    push();
    strokeWeight(2);
    for (let i = 0; i < this.onPlayers.length; ++i) {
      if (this.onPlayers[i].bSelected)
        stroke(...HIGHLIGHT_COLOR);
      else
        stroke(0);
      let color_ = color(this.onPlayers[i].color);
      color_.setAlpha(220);
      fill(color_);
      circle(coord.x + offset[i][0], coord.y + offset[i][1], size);
    }
    pop();
  }
}