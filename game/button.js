// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class Button {
  constructor(border, playerI, msg, type) {
    this.border = border;
    this.playerI = playerI;
    this.msg = msg;
    this.type = type;
  }
  
  checkAndActivate(x, y) {
    if (this.border.minx < x && x < this.border.maxx &&
        this.border.miny < y && y < this.border.maxy) {
      if (this.type != "confirm")
        this.activate();
      else {
        let names = new Set(), colors = new Set(), controls = new Set();
        
        tempPlayers.forEach(player => {
          names.add(player.name);
          colors.add(player.color);
          controls.add(player.control);
        });
        
        let playerCount = boardGame.playerCount;
        if (names.size == playerCount && colors.size == playerCount &&
            controls.size == playerCount && !names.has("Click Here to Enter Name") &&
            !colors.has("Click Here to Choose Color") &&
            !controls.has("Click Here to Choose Control"))
          this.activate();
      }
    }
  }
  
  activate() {
    if (this.type == "confirm") {
      tempPlayers.forEach(player => boardGame.players.push(
        new Player(boardGame, player.name, player.color, player.control)));
      tempPlayers = null;
      buttons = [];
      boardGame.state = EGameState.Explanation;
      boardGame.next();
      return;
    }
    
    let result;
    while (true) {
      result = prompt(this.msg);
      if (!result)
        return;
      
      let bValidInput = true;
      switch (this.type) {
        case "color":
          let realColor = new Option().style;
          realColor.color = result;
          if (realColor.color != result)
            bValidInput = false;
          break;
        case "control":
          switch (result) {
            case "w":
              result = EControl.WASD;
              break;
            case "i":
              result = EControl.IJKL;
              break;
            case "0":
              result = EControl.Arrows;
              break;
            case "8":
              result = EControl.Numpad;
              break;
            default:
              bValidInput = false;
              break;
          }
          break;
      }
      if (bValidInput)
        break;
    }
    
    tempPlayers[this.playerI][this.type] = result;
  }
}