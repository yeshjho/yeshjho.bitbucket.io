// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class BoardGame {
  constructor(playerCount) {
    this.playerCount = playerCount;
    this.players = [];
    this.cameraCenter = {x: 0, y: 0};
    
    this.cells = [];
    this.cellById = {};
    this.nextCell = {};
    
    this.state = EGameState.Intro;
    this.miniGameRank = [];
    this.miniGame = null;
    this.miniGameType = null;
    
    this.action = this.act();
    this.explanation = this.explain();
    
    this.currentExplanation = "";
    
    this.currentPlayer = null;
    this.playerTurnOrder = [];
    this.turnCount = 1;
    
    this.activeThief = null;
    
    this.winner = null;
    
    this.bDrawPlayingMoving = false;
    this.playerMovingI = NaN;
    
    this.generateCells();
  }
  
  generateCells() {
    const typeTBDCellCount = 83;
    
    let typePool = [];
    for (let i = 0; i < random(3, 6); ++i)
      typePool.push(ECellType.UFO);
    for (let i = 0; i < random(1, 4); ++i)
      typePool.push(ECellType.ResetRandomly);
    for (let i = 0; i < random(4, 7); ++i)
      typePool.push(ECellType.Shop);
    for (let i = 0; i < random(6, 12); ++i)
      typePool.push(ECellType.OneOnOneFight);
    if (this.playerCount == 4) {
      for (let i = 0; i < random(4, 7); ++i)
        typePool.push(ECellType.TwoOnTwoFight);
      for (let i = 0; i < random(3, 6); ++i)
        typePool.push(ECellType.OneOnThreeFight);
    }
    for (let i = 0; i < random(7, 13); ++i)
      typePool.push(ECellType.MoveForward);
    for (let i = 0; i < random(5, 10); ++i)
      typePool.push(ECellType.MoveBackward);
    let coinPlusCount = floor((typeTBDCellCount - typePool.length) * 2 / 3)
    for (let i = 0; i < coinPlusCount; ++i)
      typePool.push(ECellType.CoinPlus)
    let coinMinusCount = typeTBDCellCount - typePool.length
    for (let i = 0; i < coinMinusCount; ++i)
      typePool.push(ECellType.CoinMinus);
    
    // x, y, bforceTrigger, 
    // w/d/s/n for warp/diamond/seperator/nextCell, warpId | candidates | nextCellId
    for (let i = 0; i < Object.keys(board).length; ++i) {
      let x = board[i][0] * 50;
      let y = board[i][1] * 50;
      let bForceTrigger = board[i][2];
      let type = board[i][3];
      let extraValue = board[i][4];
      let bIsHidden = false;
      
      this.nextCell[i] = type == 'n' ? extraValue : i + 1;
      
      switch (type) {
        case 'w':
          type = ECellType.Warp;
          break;
        case 'd':
          type = ECellType.Diamond;
          break;
        case 's':
          type = ECellType.Seperator;
          break;
        case 'n':
        default:
          let typeIndex = floor(random(typePool.length));
          type = typePool[typeIndex];
          typePool.splice(typeIndex, 1);
          bIsHidden = random() < 0.1 ? true : false;
      }
      
      if (type == ECellType.MoveForward || type == ECellType.MoveBackward) {
        extraValue = random([1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 6])
      }
      
      let newCell = new BoardCell(this, i, x, y, 50, 50,
                                  type, bIsHidden, bForceTrigger, extraValue);
      this.cellById[i] = newCell;
      this.cells.push(newCell);
    }
  }
  
  getNextCell(id) {
    return this.nextCell[id];
  }
  
  getPrevCells(id) {
    let result = [];
    for (let key in this.nextCell) {
      if (this.nextCell[key] == id)
        result.push(key);
    }
    return result;
  }
  
  getCellById(id) {
    return this.cellById[id];
  }
  
  getCoordAccordingCamera(originalX, originalY) {
    let drawX = originalX - this.cameraCenter.x + width/2;
    let drawY = map(originalY - this.cameraCenter.y, 0, height, height, 0) - height/2;
    return {x: drawX, y: drawY};
  }
  
  nextExplanation() {
    this.explanation.next();
  }
  
  *explain() {
    let explanations = [
      "Welcome! Your goal is to get the one and only diamond" +
        "and bring it back to the start point!",
      "For the first and every " + MINIGAME_TURN_GAP +
        " turns, players play a mini game to decide the turn order.",
      "If you have a diamond, since it's very heavy, you get -1 for every dice you roll.",
      "For other players, if you arrive(stop) at the same cell where the player who has" +
        " diamond is on, you can roll the 1/2-chance dice to steal the diamond!",
      "Besides, there's an item that can steal the diamond!",
      "That's all that I can tell you. Other things? Find out for yourself!",
      "Good Luck!"
      ];
    for (let txt of explanations) {
      this.currentExplanation = txt;
      yield;
    }
    
    this.next();
    return;
  }
  
  next() {
    this.action.next();
  }
  
  *act() {
    this.currentExplanation = "Do you want to hear the explanation?\nPress Y/N";
    yield;
    
    while (true) {
      if (this.turnCount % MINIGAME_TURN_GAP == 1) {
        this.miniGame = new MiniGame(this, undefined, EMiniGameType.Individual);
        this.state = EGameState.MiniGame;
        yield;
        
        this.playerTurnOrder = [...this.miniGameRank];
        for (let i = 0; i < this.miniGameRank.length; ++i) {
          this.miniGameRank[i].order = i + 1;
          this.miniGameRank[i].coin += MINIGAME_PRIZE[this.miniGameRank.length][i];
        }
      }
      
      if (this.activeThief && this.activeThief.remainingTurns == 0) {
        for (let player of this.players) {
          if (player.bHasDiamond) {
            player.bHasDiamond = false;
            this.activeThief.player.bHasDiamond = true;
            break;
          }
        }
        this.activeThief = null;
      }
      for (let player of this.playerTurnOrder) {
        if (this.currentPlayer)
          this.currentPlayer.bSelected = false;
        this.currentPlayer = player;
        player.bSelected = true;
        
        let itemList = [];
        for (let item of player.items) {
          if (item == EItemType.Thief && this.activeThief)
            continue;
          itemList.push([item, 0]);
        }
        if (itemList.length != 0) {
          ui.onUIEnd = result => {
            switch (result) {
              case EItemType.Item:
                setTimeout(() => {
                  ui.onUIEnd = result => {
                    player.items.splice(player.items.indexOf(result), 1);
                    let diceOffset = player.bHasDiamond ? -1 : 0;
                    let notIn = [];
                    switch (result) {
                      case EItemType.Thief:
                        this.activeThief = {player: player,
                                            remainingTurns: THIEF_STEAL_TURNS};
                        break;
                      case EItemType.Add3:
                        diceOffset = 3;
                        break;
                      case EItemType.Minus3:
                        diceOffset = -3;
                        break;
                      case EItemType.OddOnly:
                        notIn = [-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10];
                        break;
                      case EItemType.EvenOnly:
                        notIn = [-9, -7, -5, -3, -1, 1, 3, 5, 7, 9];
                        break;
                    }
                    dice.onRollEnd = () => {
                      player.move(dice.result * (result == EItemType.Mul2 ? 2 : 1) + 
                                  diceOffset);
                    };
                    dice.notIn = [...notIn];
                    dice.roll(player);
                  };
                  ui.open(itemList, player, true, false);
                }, 100);
                break;
              case EItemType.Dice:
                dice.onRollEnd = () => {
                  player.move(dice.result - (player.bHasDiamond ? 1 : 0));
                };
                dice.roll(player);
                break;
            }
          };
          ui.open([[EItemType.Item, 0], [EItemType.Dice, 0]], player, true);
        }
        else {
          dice.onRollEnd = () => {
            player.move(dice.result - (player.bHasDiamond ? 1 : 0));
          };
          dice.roll(player);
        }
        yield;
      }
      this.turnCount++;
      if (this.activeThief)
        this.activeThief.remainingTurns--;
    }
  }
  
  onMiniGameEnd(rank, bRegular=true) {
    this.miniGame = null;
    if (this.miniGameType == EMiniGameType.Individual) {
      this.state = EGameState.TieBreaker;
      tieBreakerDice.onRollEnd = () => {
        this.miniGameRank = tieBreakerDice.resultRank;
        this.state = EGameState.MiniGameResult;
        setTimeout(() => {
          this.state = EGameState.BoardGame;
          if (bRegular) {
            this.next();
          }
          else {
            // Temp. Need thinking
            for (let i = 0; i < this.miniGameRank.length; ++i) {
              this.miniGameRank[i].coin += MINIGAME_PRIZE[this.miniGameRank.length][i];
            }

            let cell = this.getCellById(this.currentPlayer.cellOn);
            if (cell.onTriggerEnd) {
              cell.onTriggerEnd();
              cell.onTriggerEnd = null;
            }
          }
        }, 4000);
      };
      tieBreakerDice.roll(rank);
    }
    else {
      this.miniGameRank = [...rank];
      this.state = EGameState.MiniGameResult;
      setTimeout(() => {
        this.state = EGameState.BoardGame;
        // Temp. Need thinking
        for (let i = 0; i < 4; ++i) {
          if (this.miniGameRank[i].length == 4) {
            player.coin += MINIGAME_PRIZE[4][2];
            break;
          }
          for (let player of this.miniGameRank[i]) {
            player.coin += MINIGAME_PRIZE[4][i];
          }
        }

        let cell = this.getCellById(this.currentPlayer.cellOn);
        if (cell.onTriggerEnd) {
          cell.onTriggerEnd();
          cell.onTriggerEnd = null;
        }
      }, 4000);
    }
  }
  
  gameEnd(winner) {
    this.winner = winner;
    this.state = EGameState.GameEnd;
  }
  
  update() {
    switch (this.state) {
      case EGameState.BoardGame:
        for (let player of this.players) {
          if (player.bHasDiamond) {
            sfx.diamond_bgm.play();
            break;
          }
        }
        let cell = this.getCellById(this.currentPlayer.cellOn);
        this.cameraCenter = {x: cell.x, y: cell.y};
        break;
      case EGameState.MiniGame:
        sfx.diamond_bgm.stop();
        this.miniGame.update();
        break;
      case EGameState.GameEnd:
        sfx.diamond_bgm.stop();
        break;
    }
  }
  
  draw() {
    switch (this.state) {
      case EGameState.Intro:  // Should've made draw method in button class
        let playerSlotWidth = width / this.playerCount;
        push();
        strokeWeight(4);
        for (let j = 1; j < this.playerCount; ++j) {
          line(j * playerSlotWidth, 0, j * playerSlotWidth, height*8/9);
        }
        line(0, height*8/9, width, height*8/9);
        
        strokeWeight(2);
        imageMode(CENTER);
        let j = 0;
        for (let tempPlayer of tempPlayers) {
          let x = playerSlotWidth / 2 * (2 * j + 1);
          textAlign(CENTER, TOP);
          textSize(30);
          text("PLAYER " + (j + 1), x, 30);
          
          textAlign(CENTER, CENTER);
          textSize(14);
          text(tempPlayer.name, x, height/4);
          
          if (tempPlayer.color == "Click Here to Choose Color") {
            text(tempPlayer.color, x, height/2);
          }
          else {
            fill(tempPlayer.color);
            circle(x, height/2, playerSlotWidth/9);
          }
          
          fill(0);
          switch (tempPlayer.control) {
            case EControl.WASD:
              image(img.wasd, x, height*3/4);
              break;
            case EControl.IJKL:
              image(img.ijkl, x, height*3/4);
              break;
            case EControl.Arrows:
              image(img.arrows, x, height*3/4);
              break;
            case EControl.Numpad:
              image(img.numpad, x, height*3/4);
              break;
            default:
              text(tempPlayer.control, x, height*3/4);
              break;
          }
          j++;
        }
        
        textAlign(CENTER, CENTER);
        textSize(25);
        text("CONFIRM", width/2, height*17/18);
        pop();
        break;
      case EGameState.Explanation:
        push();
        textAlign(CENTER, CENTER);
        textSize(20);
        rectMode(CENTER);
        text(this.currentExplanation, width/2, height/2, width - 100, height - 100);
		textSize(12);
        text("Click to continue", width/2, height/2 + 20);
        pop();
        break;
      case EGameState.BoardGame:
        this.cells.forEach(cell => cell.draw());
        this.players.forEach(player => player.draw());
        this.drawHUD();
        dice.draw();
        newDice.draw();
        ui.draw();
        break;
      case EGameState.MiniGame:
        this.miniGame.draw();
        break;
      case EGameState.TieBreaker:
        tieBreakerDice.draw();
        break;
      case EGameState.MiniGameResult:
        let i;
        let bWinner = true;
        switch (this.miniGameType) {
          case EMiniGameType.Individual:
            i = 1;
            for (let player of this.miniGameRank) {
              this.drawPlayerRank(player, width*i/5, height/2, i);
              i++;
            }
            break;
          case EMiniGameType.TwoOnTwo:
          case EMiniGameType.OneOnThree:
            if (this.miniGameRank[0].length == 4) {
              for (let player of this.miniGameRank[0]) {
                this.drawPlayerRank(player, width*i/5, height/2, 3);
              }
              break;
            }
            else {
              i = 1;
              for (let place of this.miniGameRank) {
                for (let player of place) {
                  this.drawPlayerRank(player, width*i/5, height/2, bWinner ? 1 : 3);
                  i++;
                }
                if (place.length != 0)
                  bWinner = false;
              }
            }
            break;
        }
        break;
      case EGameState.GameEnd:
        push();
        fill(255);
        stroke(this.winner.color);
        strokeWeight(5);
        textAlign(CENTER, CENTER);
        textSize(40);
        text(this.winner.name + " has won!", width/2, height/2);
        let heightOffset = textDescent();
        textSize(20);
        noStroke();
        fill(0);
        text("Press Spacebar to Restart.", width/2, height/2 + heightOffset + 50);
        pop();
        break;
    }
  }
  
  drawPlayerRank(player, x, y, order) {
    push();
    stroke(player.color);
    strokeWeight(5);
    textAlign(CENTER, CENTER);
    textSize(20);
    fill(0);
    text(EOrdinal[order], x, y);

    let heightOffset = textDescent();
    textAlign(CENTER, TOP);
    textSize(30);
    fill(255);
    text(player.name, x, y + heightOffset + 10);

    // from here, is depend on the prize type. Need thinking
    stroke(0);
    strokeWeight(1);
    textAlign(CENTER, TOP);
    textSize(15);
    fill(0);
    text("+" + MINIGAME_PRIZE[this.miniGameRank.length][order-1], x, y + heightOffset + 50);
    let widthOffset = textWidth("+" + MINIGAME_PRIZE[this.miniGameRank.length][order-1]);

    imageMode(CENTER);
    image(img.coin, x + widthOffset + 10, y + heightOffset*2 + 50, 30, 30);
    pop();
  }
  
  drawHUD() {
    push();
    textAlign(LEFT, BOTTOM);
    fill(0);
    textSize(25);
    text("Turn " + this.turnCount, 10, height - 10);
    
    imageMode(CENTER);
    textAlign(LEFT, TOP);
    strokeWeight(5);
    fill(0);
    textSize(20);
    let nameEndsAt = [];
    this.players.forEach(player => nameEndsAt.push(textWidth(player.name)));
    let maxNameWidth = max(nameEndsAt);
    let orderWidth = textWidth("2nd");
    let i = 0;
    for (let player of this.playerTurnOrder) {
      stroke(player.color);
      fill(255);
      textSize(15);
      text(EOrdinal[player.order], 10, i * 30 + 44);
      if (player.bSelected)
        fill(...HIGHLIGHT_COLOR);
      else
        fill(0);
      textSize(20);
      text(player.name, 20 + orderWidth, i * 30 + 40);
      fill(0);
      text(player.coin, 20 + orderWidth + maxNameWidth * 1.3, i * 30 + 40);

      if (player.bHasDiamond)
        image(img.shining_diamond, 65 + orderWidth + maxNameWidth * 1.3,
              i * 30 + 47, 35, 35);

      i++;
    }
    stroke(0);
    strokeWeight(5);
    fill(255);
    text("Name", 20 + orderWidth, 10);
    image(img.coin, 30 + orderWidth + maxNameWidth * 1.3, 20, 32, 33);
    pop();

    if (this.bDrawPlayerMoving) {
      push();
      textSize(50);
      strokeWeight(10);
      stroke(0);
      fill(255);
      textAlign(CENTER, TOP);
      text(this.playerMovingI, width/2, 10);
      pop();
    }
    
    if (this.activeThief) {
      image(img.item_1, width - 100, 0, 50, 50);
      textAlign(RIGHT, TOP);
      textSize(20);
      text(this.activeThief.remainingTurns, width - 15, 15);
    }
  }
}