// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


function sortByScoreShorterTime(player1, player2) {
  if (player1.miniGameScore > player2.miniGameScore)
    return -1;
  if (player1.miniGameScore < player2.miniGameScore)
    return 1;
  if (player1.miniGameFinishTime < player2.miniGameFinishTime)
    return -1;
  if (player1.miniGameFinishTime > player2.miniGameFinishTime)
    return 1;
  return 0;
}

function sortByScoreLongerTime(player1, player2) {
  if (player1.miniGameScore > player2.miniGameScore)
    return -1;
  if (player1.miniGameScore < player2.miniGameScore)
    return 1;
  if (player1.miniGameFinishTime < player2.miniGameFinishTime)
    return 1;
  if (player1.miniGameFinishTime > player2.miniGameFinishTime)
    return -1;
  return 0;
}


function sortByScoreTime(players, bOnlyScore=false, bShorterIsBetter=true) {
  let rank = [[], [], [], []];
  players = [...players];
  players = players.sort(bShorterIsBetter ? sortByScoreShorterTime : sortByScoreLongerTime);
  rank[0].push(players.shift());
  let curRank = 0;
  
  let i = 0;
  for (let player of players) {
    if ((bOnlyScore ? true : abs(rank[curRank][0].miniGameFinishTime - 
                                 player.miniGameFinishTime) <= FRAME_ERROR) &&
        rank[curRank][0].miniGameScore == player.miniGameScore)
      rank[curRank].push(player);
    else {
      curRank = i + 1;
      rank[curRank].push(player);
    }
    i++;
  }
  return rank;
}


function drawLines(lineState) {
  push();
  strokeWeight(4);
  switch (lineState) {
    case EMiniGameWindow.TwoVertical:
      line(width/2, 0, width/2, height);
      break;
    case EMiniGameWindow.TwoHorizontal:
      line(0, height/2, width, height/2);
      break;
    case EMiniGameWindow.Three:
      line(width/2, height/2, width/2, height);
      line(0, height/2, width, height/2);
      break;
    case EMiniGameWindow.Four:
      line(width/2, 0, width/2, height);
      line(0, height/2, width, height/2);
      break;
  }
  pop();
}


class MiniGame {
  constructor(boardGame, players, type, title, bRegular=true) {
    for (let player of boardGame.players) {
      player.miniGameScore = 0;
      player.miniGameFinishTime = 0;
    }
    this.boardGame = boardGame;
    this.players = players || [...boardGame.players];
    // [[p, p], [p, p]] or [p, [p, p, p]] if given
    this.type = type;
    boardGame.miniGameType = type;
    this.title = title || random(Object.values(EMiniGameTitle[type]));
    this.bRegular = bRegular;
    
    this.games = [];
    this.miniGameWindow = null;
    
    this.doneGameCount = 0;
    
    this.rankSortType = [];
    
    this.bShouldPlayIntro = true;
    
    switch(this.title) {
      case EMiniGameTitle[EMiniGameType.Individual].PaddleBall:
        this.rankSortType = [true,];
        switch (this.players.length) {
          case 2:
            this.miniGameWindow = EMiniGameWindow.TwoVertical;
            break;
          case 3:
            this.miniGameWindow = EMiniGameWindow.Three;
            break;
          case 4:
            this.miniGameWindow = EMiniGameWindow.Four;
            break;
        }
        
        let lvl = random(paddleBallLevels);
        for (let i = 0; i < this.players.length; ++i) {
          this.games.push(new PaddleBallLevel(
            this, EMiniGameBorder[this.miniGameWindow][i], this.players[i], lvl));
        }
        break;
      case EMiniGameTitle[EMiniGameType.Individual].Orbital:
        this.rankSortType = [false, false];
        this.games.push(new OrbitalLevel(this, this.players));
        break;
      case EMiniGameTitle[EMiniGameType.TwoOnTwo].TwoOnTwoShooting:
        this.rankSortType = [true,];
        this.games.push(new TwoOnTwoShootingLevel(this, this.players));
        break;
      case EMiniGameTitle[EMiniGameType.OneOnThree].JumpRope:
        this.rankSortType = [true,];
        this.games.push(new JumpRopeLevel(this, this.players));
        break;  
    }
    
    this.toUpdate = [...this.games];
  }
  
  onKeyPressed(keyCode_) {
    this.toUpdate.forEach(game => game.onKeyPressed(keyCode_));
  }
  
  update() {
    if (this.toUpdate.length == 0)
      return;
    if (this.bShouldPlayIntro) {
      this.bShouldPlayIntro = false;
      bIsLooping = false;
      noLoop();
      // push();
      // stroke(0);
      // strokeWeight(10);
      // fill(255);
      // textSize(40);
      // textAlign(CENTER, CENTER);
      // text(this.bRegular ? "ORDER DECIDING MINIGAME" : "GET READY!", width/2, height/2);
      // pop();
      setTimeout(() => {loop(); bIsLooping = true;}, 2*1000);
    }
      
    if (keyIsDown(AUP))
      this.toUpdate.forEach(game => game.onKeyDown(AUP));
    if (keyIsDown(ALEFT))
      this.toUpdate.forEach(game => game.onKeyDown(ALEFT));
    if (keyIsDown(ADOWN))
      this.toUpdate.forEach(game => game.onKeyDown(ADOWN));
    if (keyIsDown(ARIGHT))
      this.toUpdate.forEach(game => game.onKeyDown(ARIGHT));
    if (keyIsDown(W))
      this.toUpdate.forEach(game => game.onKeyDown(W));
    if (keyIsDown(A))
      this.toUpdate.forEach(game => game.onKeyDown(A));
    if (keyIsDown(S))
      this.toUpdate.forEach(game => game.onKeyDown(S));
    if (keyIsDown(D))
      this.toUpdate.forEach(game => game.onKeyDown(D));
    if (keyIsDown(I))
      this.toUpdate.forEach(game => game.onKeyDown(I));
    if (keyIsDown(J))
      this.toUpdate.forEach(game => game.onKeyDown(J));
    if (keyIsDown(K))
      this.toUpdate.forEach(game => game.onKeyDown(K));
    if (keyIsDown(L))
      this.toUpdate.forEach(game => game.onKeyDown(L));
    if (keyIsDown(NUM8))
      this.toUpdate.forEach(game => game.onKeyDown(NUM8));
    if (keyIsDown(NUM4))
      this.toUpdate.forEach(game => game.onKeyDown(NUM4));
    if (keyIsDown(NUM5))
      this.toUpdate.forEach(game => game.onKeyDown(NUM5));
    if (keyIsDown(NUM6))
      this.toUpdate.forEach(game => game.onKeyDown(NUM6));
    
    this.toUpdate.forEach(game => game.update());
  }
  
  draw() {
    this.games.forEach(game => game.draw());
    
    drawLines(this.miniGameWindow);
  }
  
  gameDone(game) {
    this.doneGameCount++;
    this.toUpdate.splice(this.toUpdate.indexOf(game), 1);
    
    if (this.doneGameCount >= this.games.length) {
      this.boardGame.onMiniGameEnd(
        sortByScoreTime(
          this.type == EMiniGameType.Individual ? this.players : this.boardGame.players,
          ...this.rankSortType),
        this.bRegular);
    }
  }
}