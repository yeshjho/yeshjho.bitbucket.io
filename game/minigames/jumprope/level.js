// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


// 1 vs 3
class JumpRopeLevel extends Game {
  constructor(miniGame, team) {
    // team should be [p, [p, p, p]]
    super(miniGame, EMiniGameBorder[0], null);
    
    this.theOne = team[0];
    this.theThree = team[1];
    
    this.remainingTime = 30;
    this.timer = setInterval(() => this.remainingTime--, 1000);
    
    this.jumpers = {};
    this.aliveJumpers = [];
    
    this.playerAngles = [PI/2, PI*5/4, PI*7/4];
    for (let i = 0; i < this.theThree.length; ++i) {
      let newJumper = new JumpRopeJumper(this, this.theThree[i], this.playerAngles[i]);
      this.jumpers[this.theThree[i].control] = newJumper;
      this.aliveJumpers.push(newJumper);
    }
    
    this.bCanChangeSpeed = true;
    this.ropeSpeed = -2;  // -2, -1, 0, 1
    this.ropePreviousAngle = 0;
    this.ropeAngle = 0;
    
    this.ropeSpeedChangeCoolTime = 400;
  }
  
  changeRopeSpeed(bFaster) {
    if (this.bCanChangeSpeed) {
      this.bCanChangeSpeed = false;
      setTimeout(() => this.bCanChangeSpeed = true, this.ropeSpeedChangeCoolTime);
      this.ropeSpeed += bFaster ? 1 : -1;
      if (this.ropeSpeed < -2)
        this.ropeSpeed = -2;
      else if (this.ropeSpeed > 1)
        this.ropeSpeed = 1;
    }
  }
  
  onKeyPressed(keyCode_) {
    switch (keyCode_) {
      case W:
      case S:
        if (this.jumpers[EControl.WASD])
          this.jumpers[EControl.WASD].jump();
        break;
      case A:
        if (this.theOne.control == EControl.WASD)
          this.changeRopeSpeed(false);
        break;
      case D:
        if (this.theOne.control == EControl.WASD)
          this.changeRopeSpeed(true);
        break;
        
      case I:
      case K:
        if (this.jumpers[EControl.IJKL])
          this.jumpers[EControl.IJKL].jump();
        break;
      case J:
        if (this.theOne.control == EControl.IJKL)
          this.changeRopeSpeed(false);
        break;
      case L:
        if (this.theOne.control == EControl.IJKL)
          this.changeRopeSpeed(true);
        break;
        
      case AUP:
      case ADOWN:
        if (this.jumpers[EControl.Arrows])
          this.jumpers[EControl.Arrows].jump();
        break;
      case ALEFT:
        if (this.theOne.control == EControl.Arrows)
          this.changeRopeSpeed(false);
        break;
      case ARIGHT:
        if (this.theOne.control == EControl.Arrows)
          this.changeRopeSpeed(true);
        break;
        
      case NUM8:
      case NUM5:
        if (this.jumpers[EControl.Numpad])
          this.jumpers[EControl.Numpad].jump();
        break;
      case NUM4:
        if (this.theOne.control == EControl.Numpad)
          this.changeRopeSpeed(false);
        break;
      case NUM6:
        if (this.theOne.control == EControl.Numpad)
          this.changeRopeSpeed(true);
        break;
    }
  }
  
  update() {
    this.checkCollision();
    this.checkGameEnd();
    this.ropePreviousAngle = this.ropeAngle;
    this.ropeAngle += PI/45 * map(this.ropeSpeed, -2, 1, 0.3, 1.1);
    this.ropeAngle -= (this.ropeAngle >= 2*PI ? 2*PI : 0);
    this.aliveJumpers.forEach(jumper => jumper.update());
  }
  
  checkCollision() {
    for (let jumper of this.aliveJumpers) {
      let playerAngle = jumper.initialAngle;
      if (this.ropePreviousAngle < playerAngle && playerAngle < this.ropeAngle &&
          !jumper.bJumping) {
        sfx.ship_hit.play();
        jumper.bAlive = false;
        this.aliveJumpers.splice(this.aliveJumpers.indexOf(jumper), 1);
      }
    }
  }
  
  draw() {
    push();
    let r = max(width, height)*2;
    stroke(2);
    line(width/2, height/2,
         width/2 + r*cos(this.ropeAngle), height/2 - r*sin(this.ropeAngle));
    pop();
    Object.values(this.jumpers).forEach(jumper => jumper.draw());
    this.drawHUD();
  }
  
  drawHUD() {
    push();
    strokeWeight(2);
    textAlign(CENTER, TOP);
    textSize(30);
    stroke(0);
    strokeWeight(3);
    let gb = map(this.remainingTime, 30, 0, 255, 0);
    fill(255, gb, gb);
    text(this.remainingTime, width/2, 10);
    
    strokeWeight(2);
    switch (this.ropeSpeed) {
      case -2:
        fill(100, 100, 255);
        break;
      case -1:
        fill(100, 255, 100);
        break;
      case 0:
        fill(255, 255, 100);
        break;
      case 1:
        fill(255, 100, 100);
        break;
    }
    circle(width/2, height/2, 50);
    pop();
  }
  
  checkGameEnd() {
    if (this.aliveJumpers.length == 0 && this.remainingTime == 0) {
      this.miniGame.gameDone(this);
    }
    else if (this.aliveJumpers.length == 0) {
      this.theOne.miniGameScore = 1;
      this.miniGame.gameDone(this);
    }
    else if (this.remainingTime == 0) {
      this.theOne.miniGameScore = -1;
      this.miniGame.gameDone(this);
    }
  }
}