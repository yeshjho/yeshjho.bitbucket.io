// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class JumpRopeJumper {
  constructor(lvl, player, initialAngle) {
    this.lvl = lvl;
    this.player = player;
    
    this.initialAngle = initialAngle;
    let r = (min(width, height))/2 - 100;
    this.x = width/2 + r*cos(initialAngle);
    this.y = height/2 - r*sin(initialAngle) - 17;
    this.r = 17;
    
    this.bCanJump = true;
    this.bJumping = false;
    this.jumpI = 0;
    
    this.bAlive = true;
    
    this.jumpTime = 500;
    this.jumpCoolTime = 700;
  }
  
  jump() {
    if (this.bCanJump && !this.bJumping) {
      this.bCanJump = false;
      this.bJumping = true;
      setTimeout(() => {
        this.bJumping = false;
        this.jumpI = 0;
      }, this.jumpTime);
      setTimeout(() => this.bCanJump = true, this.jumpCoolTime);
    }
  }
  
  update() {
    if (this.bJumping)
      this.jumpI++;
  }
  
  draw() {
    let drawY = this.y - sin(map(this.jumpI, 0, 30, 0, PI, true)) * 30;
    push();
    if (this.bAlive) {
      noStroke();
      fill(150, map(drawY, this.y, this.y-30, 255, 100));
      ellipse(this.x, this.y + 15, map(drawY, this.y, this.y-30, 30, 15), 15/2);
    }
    
    stroke(2);
    let color_ = color(this.player.color);
    color_.setAlpha(this.bAlive ? 255 : 100);
    fill(color_);
    circle(this.x, drawY, this.r);
    pop();
  }
}