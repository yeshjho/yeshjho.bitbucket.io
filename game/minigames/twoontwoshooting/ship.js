// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class TwoOnTwoShootingShip {
  constructor(lvl, x, mover, shooter) {
    this.lvl = lvl;
    this.mover = mover;
    this.shooter = shooter;
    
    this.x = x;
    this.y = height/2;
    this.w = 20;
    this.h = 50;
    
    this.life = 3;
    
    this.velocityY = 0;
    this.shipSpeed = 1;
    
    this.frictionCoefficient = 0.2;
    
    this.cannonHeight = 10;
    this.cannonYOffset = 0;
    this.cannonSpeed = 0.5;
    
    this.bCanShoot = true;
  }
  
  moveCannon(bUp) {
    this.cannonYOffset += (bUp ? -1 : 1) * this.cannonSpeed;
    this.constrainCannon();
  }
  
  addVelocity(bUp) {
    this.velocityY += bUp ? -this.shipSpeed : this.shipSpeed;
  }
  
  constrainShip() {
    if (this.y - this.h/2 < 0) {
      this.y = this.h/2;
      this.velocityY = 0;
    }
    else if (this.y + this.h/2 > height) {
      this.y = height - this.h/2;
      this.velocityY = 0;
    }
  }
  
  constrainCannon() {
    if (-this.h/2 > this.cannonYOffset - this.cannonHeight/2)
      this.cannonYOffset = -this.h/2 + this.cannonHeight/2;
    else if(this.h/2 < this.cannonYOffset + this.cannonHeight/2)
      this.cannonYOffset = this.h/2 - this.cannonHeight/2;
  }
  
  shoot() {
    if (this.bCanShoot) {
      this.bCanShoot = false;
      setTimeout(() => this.bCanShoot = true, 700);
      new TwoOnTwoShootingBullet(this.lvl,
                                 this.x + (this.x < width/2 ? 1 : -1) * this.w,
                                 this.y + this.cannonYOffset,
                                 3, (this.x < width/2 ? 1 : -1) * 15);
    }
  }
  
  hurt() {
    sfx.ship_hit.play();
    this.life--;
  }
  
  update() {
    if (this.velocityY > 0) {
      this.velocityY -= this.shipSpeed * this.frictionCoefficient;
      if (this.velocityY < 0) {
        this.velocityY = 0;
      }
    }
    else if (this.velocityY < 0) {
      this.velocityY += this.shipSpeed * this.frictionCoefficient;
      if (this.velocityY > 0) {
        this.velocityY = 0;
      }
    }
    this.y += this.velocityY;
    this.constrainShip();
  }
  
  draw() {
    push();
    rectMode(CENTER);
    stroke(255);
    fill(this.mover.color);
    rect(this.x, this.y, this.w, this.h);
    
    fill(this.shooter.color);
    rect(this.x, this.y + this.cannonYOffset, this.w, this.cannonHeight);
    pop();
  }
}