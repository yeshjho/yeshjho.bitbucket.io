// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class TwoOnTwoShootingBullet {
  constructor(lvl, x, y, r, speed) {
    this.lvl = lvl;
    this.x = x;
    this.y = y;
    this.r = r;
    this.speed = speed;
    
    this.lvl.bullets.push(this);
  }
  
  update() {
    this.x += this.speed;
    this.constrain();
    this.detectCollision();
  }
  
  detectCollision() {
    for (let ship of [this.lvl.teamAShip, this.lvl.teamBShip]) {
      if (ship.x - ship.w/2 < this.x && this.x < ship.x + ship.w/2 &&
          ship.y - ship.h/2 < this.y && this.y < ship.y + ship.h/2) {
        ship.hurt();
        this.destroy();
      }
    }
    for (let bullet of this.lvl.bullets) {
      if (sq((this.x - bullet.x)) + sq((this.y - bullet.y)) < sq(this.r + bullet.r) &&
          bullet != this) {
        this.destroy();
        bullet.destroy();
      }
    }
  }
  
  constrain() {
    if (this.x + this.r < 0 || this.x - this.r > width)
      this.destroy();
  }
  
  draw() {
    push();
    stroke(255);
    fill(255);
    circle(this.x, this.y, this.r);
    pop();
  }
  
  destroy() {
    this.lvl.bullets.splice(this.lvl.bullets.indexOf(this), 1);
  }
}