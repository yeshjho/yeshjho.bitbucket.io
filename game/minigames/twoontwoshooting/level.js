// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


// 2 vs 2
class TwoOnTwoShootingLevel extends Game {
  constructor(miniGame, team) {
    super(miniGame, EMiniGameBorder[0], null);
    this.teamA = team[0];
    this.teamB = team[1];
    if (random() < 0.5)
      [this.teamA[0], this.teamA[1]] = [this.teamA[1], this.teamA[0]];
    if (random() < 0.5)
      [this.teamB[0], this.teamB[1]] = [this.teamB[1], this.teamB[0]];
    // team[0] will be control moving and team[1] will shoot
    
    this.teamAShip = new TwoOnTwoShootingShip(this, 15, ...this.teamA);
    this.teamBShip = new TwoOnTwoShootingShip(this, width - 15, ...this.teamB);
    this.bullets = [];
  }
  
  onKeyDown(key_) {
    let player;
    let bUp = false;
    switch (key_) {
      case W:
        bUp = true;
      case S:
        if (this.teamA[0].control == EControl.WASD)
          this.teamAShip.addVelocity(bUp);
        else if (this.teamA[1].control == EControl.WASD)
          this.teamAShip.moveCannon(bUp);
        else if (this.teamB[0].control == EControl.WASD)
          this.teamBShip.addVelocity(bUp);
        else if (this.teamB[1].control == EControl.WASD)
          this.teamBShip.moveCannon(bUp);
        break;
      case A:
      case D:
        if (this.teamA[1].control == EControl.WASD)
          this.teamAShip.shoot();
        else if (this.teamB[1].control == EControl.WASD)
          this.teamBShip.shoot();
        break;
        
      case AUP:
        bUp = true;
      case ADOWN:
        if (this.teamA[0].control == EControl.Arrows)
          this.teamAShip.addVelocity(bUp);
        else if (this.teamA[1].control == EControl.Arrows)
          this.teamAShip.moveCannon(bUp);
        else if (this.teamB[0].control == EControl.Arrows)
          this.teamBShip.addVelocity(bUp);
        else if (this.teamB[1].control == EControl.Arrows)
          this.teamBShip.moveCannon(bUp);
        break;
      case ALEFT:
      case ARIGHT:
        if (this.teamA[1].control == EControl.Arrows)
          this.teamAShip.shoot();
        else if (this.teamB[1].control == EControl.Arrows)
          this.teamBShip.shoot();
        break;
        
      case I:
        bUp = true;
      case K:
        if (this.teamA[0].control == EControl.IJKL)
          this.teamAShip.addVelocity(bUp);
        else if (this.teamA[1].control == EControl.IJKL)
          this.teamAShip.moveCannon(bUp);
        else if (this.teamB[0].control == EControl.IJKL)
          this.teamBShip.addVelocity(bUp);
        else if (this.teamB[1].control == EControl.IJKL)
          this.teamBShip.moveCannon(bUp);
        break;
      case J:
      case L:
        if (this.teamA[1].control == EControl.IJKL)
          this.teamAShip.shoot();
        else if (this.teamB[1].control == EControl.IJKL)
          this.teamBShip.shoot();
        break;
        
      case NUM8:
        bUp = true;
      case NUM5:
        if (this.teamA[0].control == EControl.Numpad)
          this.teamAShip.addVelocity(bUp);
        else if (this.teamA[1].control == EControl.Numpad)
          this.teamAShip.moveCannon(bUp);
        else if (this.teamB[0].control == EControl.Numpad)
          this.teamBShip.addVelocity(bUp);
        else if (this.teamB[1].control == EControl.Numpad)
          this.teamBShip.moveCannon(bUp);
        break;
      case NUM4:
      case NUM6:
        if (this.teamA[1].control == EControl.Numpad)
          this.teamAShip.shoot();
        else if (this.teamB[1].control == EControl.Numpad)
          this.teamBShip.shoot();
        break;
    }
  }
  
  update() {
    this.bullets.forEach(bullet => bullet.update());
    this.teamAShip.update();
    this.teamBShip.update();
    
    this.checkGameEnd();
  }
  
  draw() {
    background(0);
    this.teamAShip.draw();
    this.teamBShip.draw();
    this.bullets.forEach(bullet => bullet.draw());
    this.drawHUD();
  }
  
  drawHUD() {
    push();
    textAlign(LEFT, TOP);
    stroke(0);
    fill(255, 50, 50);
    textSize(30);
    text(this.teamAShip.life, 10, 10);
    
    textAlign(RIGHT, TOP);
    text(this.teamBShip.life, width-10, 10);
    pop();
  }
  
  checkGameEnd() {
    if (this.teamAShip.life <= 0 && this.teamBShip.life <= 0) {
      [...this.teamA, ...this.teamB].forEach(player => player.miniGameFinishTime = 0);
      this.miniGame.gameDone(this);
    }
    else if (this.teamAShip.life <= 0) {
      this.teamB.forEach(player => player.miniGameScore = 1);
      this.miniGame.gameDone(this);
    }
    else if (this.teamBShip.life <= 0) {
      this.teamA.forEach(player => player.miniGameScore = 1);
      this.miniGame.gameDone(this);
    }
  }
}