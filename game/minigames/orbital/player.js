// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class OrbitalPlayer {
  constructor(lvl, player, orbital, bCCW) {
    this.lvl = lvl;
    this.player = player;
    this.orbital = orbital;
    this.bCCW = bCCW;
    
    this.speedMultiplier = 1;
    this.angle = random(0, 2*PI);
    
    this.bAlive = true;
    
    let radius = this.lvl.sunSize + this.lvl.orbitalGap * (this.orbital + 1);
    this.x = width/2 + radius * cos(this.angle);
    this.y = height/2 + radius * sin(this.angle);
    this.r = this.lvl.sunSize / 3.5;
  }
  
  moveOrbital(bUp) {
    if ((this.orbital == this.lvl.orbitalCount - 1 && bUp) || (this.orbital == 0 && !bUp))
      return;
    else
      this.orbital += bUp ? 1 : -1;
  }
  
  update() {
    this.angle += (this.bCCW ? 1 : -1) * PI/150 * this.speedMultiplier;
    let radius = this.lvl.sunSize + this.lvl.orbitalGap * (this.orbital + 1);
    this.x = width/2 + radius * cos(this.angle);
    this.y = height/2 + radius * sin(this.angle);
    
    let otherPlayers = [...this.lvl.alivePlayers];
    otherPlayers.splice(otherPlayers.indexOf(this), 1);
    for (let player of otherPlayers) {
      if (sq((this.x - player.x)) + sq((this.y - player.y)) < sq(this.r + player.r)) {
        this.destroy();
        player.destroy();
      }
    }
    for (let asteroid of this.lvl.asteroids) {
      if (sq((this.x - asteroid.x)) + sq((this.y - asteroid.y)) < sq(this.r + asteroid.r)) {
        this.destroy();
      }
    }
  }
  
  draw() {
    push();
    let color_ = color(this.player.color);
    color_.setAlpha(this.bAlive ? 255 : 100);
    fill(color_);
    circle(this.x, this.y, this.r);
    pop();
  }
  
  destroy() {
    this.lvl.alivePlayers.splice(this.lvl.alivePlayers.indexOf(this), 1);
    this.player.miniGameFinishTime = frameCount;
    this.bAlive = false;
    sfx.explosion.play();
  }
}