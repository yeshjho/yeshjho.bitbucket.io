// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


// individual
class OrbitalLevel extends Game {
  constructor(miniGame, players) {
    super(miniGame, EMiniGameBorder[0], null);
    switch (players.length) {
      case 2:
        this.orbitalCount = floor(random(3, 5));
        break;
      case 3:
        this.orbitalCount = floor(random(4, 6));
        break;
      case 4:
        this.orbitalCount = floor(random(5, 8));
        break;
    }
    players.forEach(player => player.miniGameScore = 0);
    this.players = {};
    this.alivePlayers = [];
    this.asteroids = [];
    this.items = [];
    
    this.frames = 0;
    
    this.sunSize = min(width, height) / 20;
    this.orbitalGap = (min(width, height) - this.sunSize) / 2 /
      (this.orbitalCount + 1);
    
    let orbitalPool = [...Array(this.orbitalCount).keys()];
    let i = 0;
    for (let player of players) {
      let orbital = random(orbitalPool);
      let newPlayer = new OrbitalPlayer(this, player, orbital, i % 2 == 0 ? true : false);
      this.players[player.control] = newPlayer;
      this.alivePlayers.push(newPlayer);
      orbitalPool.splice(orbitalPool.indexOf(orbital), 1);
      i++;
    }
    
    sfx.orbital_bgm.loop();
  }
  
  onKeyPressed(key_) {
    let player;
    let bUp = false;
    switch (key_) {
      case W:
        bUp = true;
      case S:
        player = this.players[EControl.WASD];
        break;
        
      case AUP:
        bUp = true;
      case ADOWN:
        player = this.players[EControl.Arrows];
        break;
        
      case I:
        bUp = true;
      case K:
        player = this.players[EControl.IJKL];
        break;
        
      case NUM8:
        bUp = true;
      case NUM5:
        player = this.players[EControl.Numpad];
        break;
    }
    
    if (player) {
      player.moveOrbital(bUp);
    }
  }
  
  update() {
    this.frames++;
    
    this.summonAsteroid();
    
    this.alivePlayers.forEach(player => player.update());
    this.asteroids.forEach(asteroid => asteroid.update());
    this.items.forEach(item => item.update());
    
    this.checkGameEnd();
  }
  
  draw() {
    this.drawBackground();
    Object.values(this.players).forEach(player => player.draw());
    this.asteroids.forEach(asteroid => asteroid.draw());
    this.items.forEach(item => item.draw());
  }
  
  drawBackground() {
    push();
    background(0);
    fill(255, (sin(millis()/500) + 1) * 50, 0);
    stroke(255);
    circle(width/2, height/2, this.sunSize);
    
    noFill();
    for (let i = 1; i <= this.orbitalCount; ++i) {
      circle(width/2, height/2, this.sunSize + this.orbitalGap * i);
    }
    pop();
  }
  
  summonAsteroid() {
    if (random() < map(this.frames, 0, 2000, 0.003, 0.01)) {
      let asteroidX, asteroidY;
      if (random() < 0.5) {
        asteroidX = random() < 0.5 ? 0 : width;
        asteroidY = random(0, height);
      }
      else {
        asteroidX = random(0, width);
        asteroidY = random() < 0.5 ? 0 : height;
      }
      this.asteroids.push(new OrbitalAsteroid(this, asteroidX, asteroidY,
                                              random(10, min(width, height)/23),
                                              random(1, 10)));
    }
  }
  
  checkGameEnd() {
    if (this.alivePlayers.length <= 1) {
      if (this.alivePlayers[0])
        this.alivePlayers[0].player.miniGameScore = 1;
      this.miniGame.gameDone(this);
      sfx.orbital_bgm.stop();
    }
  }
}