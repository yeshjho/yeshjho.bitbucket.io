// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class OrbitalAsteroid {
  constructor(lvl, x, y, r, speed) {
    this.lvl = lvl;
    this.x = x;
    this.y = y;
    this.r = r;
    this.speed = speed;
    
    this.angle = atan2(height/2 - this.y, width/2 - this.x);
  }
  
  update() {
    this.x += cos(this.angle) * this.speed;
    this.y += sin(this.angle) * this.speed;
    
    if (sq((this.x - width/2)) + sq((this.y - height/2)) < sq(this.r + this.lvl.sunSize)) {
      this.destroy();
    }
  }
  
  draw() {
    push();
    fill(92, 64, 51);
    circle(this.x, this.y, this.r);
    pop();
  }
  
  destroy() {
    this.lvl.asteroids.splice(this.lvl.asteroids.indexOf(this), 1);
  }
}