// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


const RED = [255, 50, 50];
const GREEN = [50, 255, 50];
const BLUE = [50, 50, 255];

const EPaddleBallItem = {
  none: 0,
  paddleSpeedUp: 1,
  paddleSpeedDown: -1,
  paddleSizeUp: 2,
  paddleSizeDown: -2,
  ballPlus: 3,
  ballSpeedUp: 4,
  ballSpeedDown: -4,
  shield: 5,
  // paddlePlus: 6,
  ballSizeUp: 7,
  ballSizeDown: -7
};

const EPaddleBallItemColor = {
  1: GREEN,
  "-1": RED,
  2: GREEN,
  "-2": RED,
  3: GREEN,
  4: GREEN,
  "-4": RED,
  5: BLUE,
  6: GREEN,
  7: GREEN,
  "-7": RED
};

const EPaddleBallItemIcon = {
  1: "PS+",
  "-1": "PS-",
  2: "PZ+",
  "-2": "PZ-",
  3: "B+",
  4: "BS+",
  "-4": "BS-",
  5: "SH",
  6: "P+",
  7: "BZ+",
  "-7": "BZ-",
};

const MU_S = 0.6;
const MU_K = 0.3;

const MIN_BALL_RADIUS = 5;
const MAX_BALL_RADIUS = 20;