// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class PaddleBallBrick {
  constructor(lvl, x, y, w, h, color, item) {
    this.lvl = lvl;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.color = color ? color : [255, 100, 100];
    
    this.item = item;
    if (item === undefined && random(100) < this.lvl.lvl_info.item_chance) {
      this.item = random(Object.values(EPaddleBallItem));
    }
    
    this.timeouts = [];
    
    this.lvl.bricks.push(this);
  }

  get tl() {
    return {
      x: this.x - this.w / 2,
      y: this.y - this.h / 2
    };
  }

  get br() {
    return {
      x: this.x + this.w / 2,
      y: this.y + this.h / 2
    };
  }

  collided() {
    this.destroy();
    this.lvl.player.miniGameScore++;
    if (this.item)
      new PaddleBallItem(this.lvl, this.x, this.y, this.item);
  }

  draw() {
    push();
    rectMode(CENTER);
    fill(this.color);
    rect(this.x, this.y, this.w, this.h);
    pop();
  }
  
  destroy() {
    this.lvl.bricks.splice(this.lvl.bricks.indexOf(this), 1);
    this.timeouts.forEach(timeout => clearTimeout(timeout));
  }
}