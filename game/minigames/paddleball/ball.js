// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


function getX(y, pointA, pointB) {
  return (pointB.x - pointA.x) * (y - pointA.y) / (pointB.y - pointA.y) + pointA.x;
}

function getY(x, pointA, pointB) {
  return (pointB.y - pointA.y) * (x - pointA.x) / (pointB.x - pointA.x) + pointA.y;
}

function isInBrick(point, brick) {
  return brick.tl.x <= point.x && point.x <= brick.br.x &&
         brick.tl.y <= point.y && point.y <= brick.br.y
}

function isInBall(point, brick, ball) {
  if (isInBrick(point, brick)) {
    return (ball.x - point.x) ** 2 + (ball.y - point.y) ** 2 <= ball.r ** 2;
  }
  return false;
}

function getIntersectingPoints(pointA, pointB, brick) { //pointB is previous
  let points = [];
  
  let topY = brick.tl.y;
  let bottomY = brick.br.y;
  let leftX = brick.tl.x;
  let rightX = brick.br.x;
  
  let candidates = {top: {x: getX(topY, pointA, pointB), y: topY},
                    bottom: {x: getX(bottomY, pointA, pointB), y: bottomY},
                    left: {x: leftX, y: getY(leftX, pointA, pointB)},
                    right: {x: rightX, y: getY(rightX, pointA, pointB)}}
  Object.entries(candidates).forEach((side, point) => {
    if (!isInBrick(point, brick))
      return;
    switch(side) {
      case top:
        if (!(pointB.y < topY && topY < pointA.y))
          return;
        break;
      case bottom:
        if (!(pointA.y < bottomY && bottomY < pointB.y))
          return;
        break;
      case left:
        if (!(pointB.x < leftX && leftX < pointB.x))
          return;
        break;
      case right:
        if (!(pointA.x < rightX && rightX < pointA.x))
          return;
        break;
    }
    points.push(point);
  })
  
  return points;
}

function sortByXValue(pointA, pointB) {
  if (pointA.x < pointB.x)
    return -1;
  if (pointA.x > pointB.x)
    return 1;
  return 0;
}

function sortByYValue(brickA, brickB) {
  if (brickA.y < brickB.y)
    return -1;
  if (brickA.y > brickB.y)
    return 1;
  return 0;
}

function getSide(point, brick) {
  if (point.y == brick.tl.y)
    return EBrickSide.TOP;
  if (point.y == brick.br.y)
    return EBrickSide.BOTTOM;
  if (point.x == brick.tl.x)
    return EBrickSide.LEFT;
  if (point.x == brick.br.x)
    return EBrickSide.RIGHT;
}

const EBrickSide = {
  TOP: 0,
  BOTTOM: 1,
  LEFT: 2,
  RIGHT: 3
}

class PaddleBallBall {
  constructor(lvl, velocity, x, y, r, color=[255, 255, 0]) {
    this.lvl = lvl;
    this.x = x;
    this.px = NaN;
    this.y = y;
    this.py = NaN;
    this.r = r;
    this.color = color;
    this.velocity = velocity;
    
    this.timeouts = [];
    
    this.lvl.balls.push(this);
  }
  
  get r() {
    return this._r;
  }
  
  set r(r) {
    if (r < MIN_BALL_RADIUS)
      r = MIN_BALL_RADIUS;
    else if (r > MAX_BALL_RADIUS)
      r = MAX_BALL_RADIUS;
    this._r = r;
  }

  update() {
    this.px = this.x;
    this.py = this.y;
    this.x += this.velocity.x;
    this.y += this.velocity.y;

    this.constrain();
    this.detectCollision();
  }

  // sfx
  constrain() {
    if (this.x - this.r < this.lvl.border.minx) {
      this.x = this.lvl.border.minx + this.r;
      this.velocity.x *= -1;
      
      sfx.ballBounce.play();
    }
    else if (this.x + this.r > this.lvl.border.maxx) {
      this.x = this.lvl.border.maxx - this.r;
      this.velocity.x *= -1;
      
      sfx.ballBounce.play();
    }
    if (this.y - this.r < this.lvl.border.miny) {
      this.y = this.lvl.border.miny + this.r;
      this.velocity.y *= -1;
      
      sfx.ballBounce.play();
    }
    else if (this.y + this.r > this.lvl.border.maxy) {
      this.destroy();
      //this.y = height - this.r;
      //this.velocity.y *= -1;
    }
  }

  detectCollision() {
    let bricksToJudge = [...this.lvl.paddles, ...this.lvl.bricks];
    this.lvl.shields.forEach(shield => {if (shield) bricksToJudge.push(shield);})
    let ballsToJudge = [...this.lvl.balls];
    ballsToJudge.splice(ballsToJudge.indexOf(this), 1);
    
    let collidedBricks = [];
    let collidedBrick;
    
    for (let brick of bricksToJudge) {
      
      let topY = brick.tl.y;
      let bottomY = brick.br.y;
      let leftX = brick.tl.x;
      let rightX = brick.br.x;

      let topPoint = {
        x: getX(topY, brick, this),
        y: topY
      };
      let bottomPoint = {
        x: getX(bottomY, brick, this),
        y: bottomY
      };
      let leftPoint = {
        x: leftX,
        y: getY(leftX, brick, this)
      };
      let rightPoint = {
        x: rightX,
        y: getY(rightX, brick, this)
      };
      
      if ((isInBrick({x: this.x, y: this.y + this.r}, brick) &&
           !(isInBrick({x: this.x, y: this.y}, brick))) ||
          isInBall(topPoint, brick, this) ||
          (isInBrick({x: getX(topY, {x: this.px, y: this.py}, this), y: topY}, brick) &&
           this.py < topY && topY < this.y)) {
        this.py = this.y;
        this.y = brick.tl.y - this.r;
        if (brick instanceof PaddleBallPaddle) {
          this.velocity.y *= -map(abs(brick.velocityX), 0, 20, 0.85, 2);
          this.velocity.x += brick.velocityX * MU_K;
          
          sfx.ballBounce.play();
        }
        else {
          this.velocity.y *= -1;
          
          collidedBricks.push(brick);
        }
      }
      else if ((isInBrick({x: this.x, y: this.y - this.r}, brick) &&
                !(isInBrick({x: this.x, y: this.y}, brick))) ||
               isInBall(bottomPoint, brick, this) ||
               (isInBrick({x: getX(bottomY, {x: this.px, y: this.py}, this),
                           y: bottomY}, brick) &&
                this.y < bottomY && bottomY < this.py)) {
        this.py = this.y;
        this.y = brick.br.y + this.r;
        this.velocity.y *= -1;
        
        if (brick instanceof PaddleBallPaddle) {
          sfx.ballBounce.play();
        }
        else {
          collidedBricks.push(brick);
        }
      }
      if ((isInBrick({x: this.x + this.r, y: this.y}, brick) &&
           !(isInBrick({x: this.x, y: this.y}, brick))) ||
          isInBall(leftPoint, brick, this) ||
          (isInBrick({x: leftX, y: getY(leftX, {x: this.px, y: this.py}, this)}, brick) &&
           this.px < leftX && leftX < this.x)) {
        this.px = this.x;
        this.x = brick.tl.x - this.r;
        this.velocity.x *= -1;
        
        if (brick instanceof PaddleBallPaddle) {
          sfx.ballBounce.play();
        }
        else {
          collidedBricks.push(brick);
        }
      }
      else if ((isInBrick({x: this.x - this.r, y: this.y}, brick) &&
               !(isInBrick({x: this.x, y: this.y}, brick))) ||
               isInBall(rightPoint, brick, this) ||
               (isInBrick({x: rightX, y: getY(rightX,
                                              {x: this.px, y: this.py}, this)}, brick) && 
                this.x < rightX && rightX < this.px)) {
        this.px = this.x;
        this.x = brick.br.x + this.r;
        this.velocity.x *= -1;
        
        if (brick instanceof PaddleBallPaddle) {
          sfx.ballBounce.play();
        }
        else {
          collidedBricks.push(brick);
        }
      }
    }
    
    if (collidedBricks.length != 0) {
        sfx.ballBounce.play();
        collidedBricks.sort(sortByXValue);
        collidedBrick = this.velocity.y < 0 ?
                        collidedBricks[collidedBricks.length - 1] : collidedBricks[0];
        collidedBrick.collided();
    }

    for (let ball of ballsToJudge) {
      if ((this.x - ball.x) ** 2 + (this.y - ball.y) ** 2 < (this.r + ball.r) ** 2) {
        let angle = atan2(this.y - ball.y, this.x - ball.x);
        let meanVelocity = (this.velocity.getLength() + ball.velocity.getLength()) / 2;

        this.velocity.setAngle(angle);
        ball.velocity.setAngle(angle + PI);
        this.velocity.setLength(meanVelocity);
        ball.velocity.setLength(meanVelocity);
        
        sfx.ballBounce.play();
      }
    }
  }

  draw() {
    push();
    fill(this.color);
    circle(this.x, this.y, this.r);
    pop();
  }
  
  destroy() {
    this.lvl.balls.splice(this.lvl.balls.indexOf(this), 1);
    this.timeouts.forEach(timeout => clearTimeout(timeout));
  }
}