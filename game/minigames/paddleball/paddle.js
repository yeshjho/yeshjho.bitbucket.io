// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class PaddleBallPaddle extends PaddleBallBrick {
  constructor(lvl, x, y, w, h, color = [255, 255, 255]) {
    super(lvl, x, y, w, h, color, EPaddleBallItem.none);
    this.lvl.bricks.splice(this.lvl.bricks.indexOf(this), 1);
    this.lvl.paddles.push(this);
    
    this.velocityX = 0;
    this.paddleSpeed = 3;
  }

  addVelocity(isLeft) {
    this.velocityX += isLeft ? -this.paddleSpeed : this.paddleSpeed;
  }

  update() {
    this.x += this.velocityX;

    if (this.velocityX > 0) {
      this.velocityX -= this.paddleSpeed * MU_S;
      if (this.velocityX < 0) {
        this.velocityX = 0;
      }
    }
    else if (this.velocityX < 0) {
      this.velocityX += this.paddleSpeed * MU_S;
      if (this.velocityX > 0) {
        this.velocityX = 0;
      }
    }

    this.constrain();
  }

  constrain() {
    if (this.tl.x < this.lvl.border.minx) {
      this.x = this.lvl.border.minx + this.w / 2;
      this.velocityX = 0;
    }
    else if (this.br.x > this.lvl.border.maxx) {
      this.x = this.lvl.border.maxx - this.w / 2;
      this.velocityX = 0;
    }
  }

  useItem(item) {
    // NEED TO BE REFACTORED (SFX)
    if (sfx[item])
      sfx[item].play();
    switch (item) {
      case EPaddleBallItem.paddleSpeedUp:
        this.paddleSpeed *= 1.5;
        this.timeouts.push(setTimeout(() => this.paddleSpeed /= 1.5, 10 * 1000));
        break;
      case EPaddleBallItem.paddleSpeedDown:
        this.paddleSpeed /= 1.5;
        this.timeouts.push(setTimeout(() => this.paddleSpeed *= 1.5, 10 * 1000));
        break;
      case EPaddleBallItem.paddleSizeUp:
        this.w *= 1.5;
        this.timeouts.push(setTimeout(() => this.w /= 1.5, 10 * 1000));
        break;
      case EPaddleBallItem.paddleSizeDown:
        this.w /= 1.5;
        this.timeouts.push(setTimeout(() => this.w *= 1.5, 10 * 1000));
        break;
      case EPaddleBallItem.ballPlus:
        new PaddleBallBall(this.lvl, new PaddleBallVector(this.lvl, 0, -1),
                           this.x, this.tl.y - 10, 10);
        break;
      case EPaddleBallItem.ballSpeedUp:
        this.lvl.balls.forEach(ball =>
                               ball.velocity.setLength(ball.velocity.getLength() * 1.5));
        break;
      case EPaddleBallItem.ballSpeedDown:
        this.lvl.balls.forEach(ball =>
                               ball.velocity.setLength(ball.velocity.getLength() / 1.5));
        break;
      case EPaddleBallItem.ballSizeUp:
        this.lvl.balls.forEach(ball => ball.r *= 1.5);
        this.lvl.balls.forEach(ball =>
                               ball.timeouts.push(setTimeout(() =>
                                                             ball.r /= 1.5, 10 * 1000)));
        break;
      case EPaddleBallItem.ballSizeDown:
        this.lvl.balls.forEach(ball => ball.r /= 1.5);
        this.lvl.balls.forEach(ball =>
                               ball.timeouts.push(setTimeout(() =>
                                                             ball.r *= 1.5, 10 * 1000)));
        break;
      case EPaddleBallItem.shield:
        let shieldWidth = this.lvl.border.w / this.lvl.lvl_info.shield_count;
        for (let i = 0; i < this.lvl.lvl_info.shield_count; ++i) {
          if (!this.lvl.shields[i])
            new PaddleBallShield(this.lvl,
                                 this.lvl.border.minx + (2 * i + 1) * shieldWidth / 2, 
                                 this.lvl.border.maxy - 10,
                                 shieldWidth, 20, i);
        }
        break;
      case EPaddleBallItem.paddlePlus:
        let width_ = this.lvl.border.maxx;
        let newPaddle = new PaddleBallPaddle(this.lvl,
                                   this.x >= width_ / 2 ? width_ / 4 : width_ * 3 / 4,
                                   this.lvl.border.miny + this.lvl.border.h * 4.5 / 5,
                                   this.lvl.lvl_info.paddle_width, 25,
                                   [255, 255, 255, 190]);
        newPaddle.timeouts.push(setTimeout(() => newPaddle.destroy(), 10 * 1000));
        break;
    }
  }
  
  collided() {}
  
  destroy() {
    this.lvl.paddles.splice(this.lvl.paddles.indexOf(this), 1);
    this.timeouts.forEach(timeout => clearTimeout(timeout));
  }
}