// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class PaddleBallShield extends PaddleBallBrick {
  constructor(lvl, x, y, w, h, i, color = [173, 216, 230]) {
    super(lvl, x, y, w, h, color, EPaddleBallItem.none);
    
    this.lvl.bricks.splice(this.lvl.bricks.indexOf(this), 1);
    this.lvl.shields[i] = this;
    
    this.i = i;
  }
  
  collided() {
    delete this.lvl.shields[this.i];
  }
}