// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


function toDegrees(radians) {
  return radians / PI * 180;
}

class PaddleBallVector {
  constructor(lvl, x = 0, y = 0) {
    this.lvl = lvl;
    this.x = x;
    this.y = y;
  }

  get x() {
    return this._x;
  }

  set x(x) {
    this._x = this.constrain(x);
  }

  get y() {
    return this._y;
  }

  set y(y) {
    this._y = this.constrain(y);
  }
  
  constrain(v) {
    let mBS = this.lvl.lvl_info.minimum_ball_speed;
    let MBS = this.lvl.lvl_info.maximum_ball_speed;
    
    if (v == 0)
      v = (random() < 0.5 ? -1 : 1) * mBS;
    else if (0 < v && v < mBS)
      v = mBS;
    else if (-mBS < v && v < 0)
      v = -mBS;
    else if (v > MBS)
      v = MBS;
    else if (v < -MBS)
      v = -MBS;
    return v;
  }

  getAngle() {
    return atan2(this.y, this.x);
  }

  getDegreeAngle() {
    let angle = this.getAngle();
    return angle >= 0 ? toDegrees(angle) : toDegrees(angle + 2 * PI);
  }

  setAngle(angle_in_radians) {
    let length = this.getLength()
    this.x = length * cos(angle_in_radians);
    this.y = length * sin(angle_in_radians);
  }

  getLength() {
    return sqrt(this.x ** 2 + this.y ** 2);
  }

  setLength(length) {
    let angle = this.getAngle()
    this.x = length * cos(angle);
    this.y = length * sin(angle);
  }

  add(v2) {
    return new PaddleBallVector(this.lvl, this.x + v2.x, this.y + v2.y);
  }

  addTo(v2) {
    this.x += v2.x;
    this.y += v2.y;
  }

  subtract(v2) {
    return new PaddleBallVector(this.lvl, this.x - v2.x, this.y - v2.y);
  }

  subtractFrom(v2) {
    this.x -= v2.x;
    this.y -= v2.y;
  }

  multiply(scalar) {
    return new PaddleBallVector(this.lvl, this.x * scalar, this.y * scalar);
  }

  multiplyBy(scalar) {
    this.x *= scalar;
    this.y *= scalar;
  }

  divide(scalar) {
    return new PaddleBallVector(this.lvl, this.x / scalar, this.y / scalar);
  }

  divideBy(scalar) {
    this.x /= scalar;
    this.y /= scalar;
  }
}