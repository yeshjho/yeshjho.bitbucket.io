// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class PaddleBallItem {
  constructor(lvl, x, y, item) {
    this.lvl = lvl;
    this.x = x;
    this.y = y;
    this.r = 10;
    this.color = EPaddleBallItemColor[item];
    this.item = item;
    
    this.lvl.items.push(this);
  }

  update() {
    this.py = this.y;
    this.y += this.lvl.lvl_info.item_speed;
    if(this.y-this.r > this.lvl.border.maxy)
      this.destroy();

    this.detectCollision();
  }

  // this.lvl.border.
  detectCollision() {
    for (let paddle of this.lvl.paddles) {
      let topY = paddle.tl.y;
      let bottomY = paddle.br.y;
      let leftX = paddle.tl.x;
      let rightX = paddle.br.x;

      let topPoint = {
        x: getX(topY, paddle, this),
        y: topY
      };
      let bottomPoint = {
        x: getX(bottomY, paddle, this),
        y: bottomY
      };
      let leftPoint = {
        x: leftX,
        y: getY(leftX, paddle, this)
      };
      let rightPoint = {
        x: rightX,
        y: getY(rightX, paddle, this)
      };
      
      if (isInBrick({x: this.x, y: this.y + this.r}, paddle) ||
          isInBrick({x: this.x, y: this.y - this.r}, paddle) ||
          isInBall(topPoint, paddle, this) || isInBall(bottomPoint, paddle, this) ||
          isInBall(leftPoint, paddle, this) || isInBall(rightPoint, paddle, this) || 
          (isInBrick({x: getX(topY, {x: this.x, y: this.py}, this), y: topY}, paddle) &&
           this.py < topY && topY < this.y)) {
        this.destroy();
        paddle.useItem(this.item);
      }
    }
  }

  draw() {
    push();
    fill(this.color);
    circle(this.x, this.y, this.r);
    fill(0);
    textAlign(CENTER, CENTER);
    text(EPaddleBallItemIcon[this.item], this.x, this.y);
    pop();
  }
  
  destroy() {
    this.lvl.items.splice(this.lvl.items.indexOf(this), 1);
  }
}