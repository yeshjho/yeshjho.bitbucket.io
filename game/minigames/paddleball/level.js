// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


// individual
class PaddleBallLevel extends Game {
  constructor(miniGame, border, player, lvl_info) {
    super(miniGame, border, player.control);

    this.paddles = [];
    this.balls = [];
    this.bricks = [];
    this.shields = [];
    this.items = [];
    this.timeouts = [];
    
    this.player = player;
    this.lvl_info = lvl_info;
    
    this.generateLevel();
  }
  
  generateLevel() {
    push();
    let paddleColor = color(this.player.color);
    let ballColor = color(this.player.color);
    colorMode(HSB);
    paddleColor = color(hue(paddleColor),
                        saturation(paddleColor) * 0.3, brightness(paddleColor) * 1.2);
    ballColor = color(hue(ballColor),
                      saturation(ballColor) * 0.7, brightness(ballColor) * 0.9);
    pop();
    
    new PaddleBallPaddle(this, this.border.x,
                         this.border.miny + this.border.h * 4.5 / 5, 
                         this.lvl_info.paddle_width * this.border.w / 800, 25, 
                         [red(paddleColor), green(paddleColor), blue(paddleColor)]);
    
    let MBS = this.lvl_info.maximum_ball_speed;
    let mBS = this.lvl_info.minimum_ball_speed;
    let BR = this.lvl_info.ball_radius;
    for (let i = 0; i < this.lvl_info.ball_count; ++i) {
      new PaddleBallBall(this, new PaddleBallVector(this, 0, random(-mBS, -mBS)),
                         random(this.border.minx + BR, this.border.maxx - BR),
                         this.border.miny + this.border.h * 4 / 5, BR,
                         [red(ballColor), green(ballColor), blue(ballColor)]);
    }
    
    let column = this.lvl_info.brick_column;
    let row = this.lvl_info.brick_row;
    let col_gap = this.lvl_info.column_gap;
    let row_gap = this.lvl_info.row_gap;
    let brick_width = (this.border.w - (column - 1) * col_gap) / column;
    let brick_height = this.lvl_info.brick_height;
    while (brick_height * row + row_gap * (row - 1) > this.border.h * 4 / 5 - BR - 100) {
      row_gap *= 0.9;
      brick_height *= 0.9;
    }
    for (let i = 0; i < column; ++i) {
      for (let j = 0; j < row; ++j) {
        new PaddleBallBrick(this,
                            this.border.minx + i * col_gap + (2 * i + 1) * brick_width / 2,
                            this.border.miny + j * row_gap + (2 * j + 1) * brick_height / 2,
                            brick_width, brick_height, this.player.color);
      }
    }
  }
  
  onKeyDown(key_) {
    switch (this.control) {
      case EControl.WASD:
        switch (key_) {
          case A:
            this.paddles.forEach(paddle => paddle.addVelocity(true));
            break;
          case D:
            this.paddles.forEach(paddle => paddle.addVelocity(false));
            break;
        }
        break;
      case EControl.Arrows:
        switch (key_) {
          case ALEFT:
            this.paddles.forEach(paddle => paddle.addVelocity(true));
            break;
          case ARIGHT:
            this.paddles.forEach(paddle => paddle.addVelocity(false));
            break;
        }
        break;
      case EControl.IJKL:
        switch (key_) {
          case J:
            this.paddles.forEach(paddle => paddle.addVelocity(true));
            break;
          case L:
            this.paddles.forEach(paddle => paddle.addVelocity(false));
            break;
        }
        break;
      case EControl.Numpad:
        switch (key_) {
          case NUM4:
            this.paddles.forEach(paddle => paddle.addVelocity(true));
            break;
          case NUM6:
            this.paddles.forEach(paddle => paddle.addVelocity(false));
            break;
        }
        break;
    }
  }

  update() {
    this.paddles.forEach(paddle => paddle.update());
    this.balls.forEach(ball => ball.update());
    this.items.forEach(item => item.update());
    
    this.checkGameEnd();
  }

  draw() {
    push();
    fill(255);
    rectMode(CENTER);
    rect(...this.border.center, this.border.w, this.border.h);
    pop();
    push();
    strokeWeight(2);
    this.paddles.forEach(paddle => paddle.draw());
    this.bricks.forEach(brick => brick.draw());
    this.shields.forEach(shield => {if (shield) shield.draw();});
    this.balls.forEach(ball => ball.draw());
    this.items.forEach(item => item.draw());
    pop();
    push();
    fill(255);
    stroke(this.player.color);
    strokeWeight(8);
    textAlign(CENTER, CENTER);
    textSize(40);
    text(this.player.miniGameScore, ...this.border.center)
    pop();
  }
  
  checkGameEnd() {
    if (this.bricks.length == 0) {
      this.timeouts.forEach(timeout => clearTimeout(timeout));
      this.destroy();
    }
    else if (this.balls.length == 0) {
      this.timeouts.forEach(timeout => clearTimeout(timeout));
      this.destroy();
    }
  }
  
  destroy() {
    this.player.miniGameFinishTime = frameCount;
    this.miniGame.gameDone(this);
  }
}