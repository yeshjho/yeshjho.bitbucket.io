// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class Player {
  constructor(boardGame, name, color, control) {
    this.boardGame = boardGame;
    this.name = name;
    this.color = color;
    this.control = control;
    
    this.order = NaN;
    
    this._coin = 10;
    this._cellOn = 0;
    this.bHasDiamond = false;
    this.items = [];
    this.beforeHellWarpPosition = NaN;
    
    this.bSelected = false;
    
    this.miniGameScore = 0;
    this.miniGameFinishTime = 0;
    
    this.boardGame.getCellById(0).onPlayers.push(this);
  }
  
  get coin() {
    return this._coin;
  }
  
  set coin(value) {
    if (value < 0)
      value = 0;
    this._coin = value;
  }
  
  get cellOn() {
    return this._cellOn;
  }
  
  set cellOn(value) {
    let prevCell = this.boardGame.getCellById(this._cellOn);
    prevCell.onPlayers.splice(prevCell.onPlayers.indexOf(this), 1);
    this._cellOn = value;
    let currentCell = this.boardGame.getCellById(this._cellOn);
    currentCell.onPlayers.push(this);
  }
  
  move(amount, bTrigger=true) {
    let i = 0;
    this.boardGame.playerMovingI = amount;
    this.boardGame.bDrawPlayerMoving = true;
    if (amount == 0) {
      setTimeout(() => this.moveEnded(), 700);
    }
    else if (amount > 0) {
      let moveInterval = setInterval(() => {
        i++;
        if (i > amount) {  // Ended moving
          let cell = this.boardGame.getCellById(this.cellOn);
          if (bTrigger) {
            cell.onTriggerEnd = () => {
              this.moveEnded();
            };
            cell.trigger(this);
          }
          else {
            this.moveEnded();
          }
          clearInterval(moveInterval);
        }
        else {  // Move by 1
          sfx.move.play();
          this.boardGame.playerMovingI = amount - i;
          this.cellOn = this.boardGame.getNextCell(this.cellOn);
          let cell = this.boardGame.getCellById(this.cellOn);
          if (cell.bForceTrigger) {  // Force Trigger
            cell.onTriggerEnd = () => {this.move(amount - i, bTrigger)};
            cell.trigger(this);
            clearInterval(moveInterval);
          }
        }
      }, 500);
    }
    else {
      let moveInterval = setInterval(() => {
        i--;
        if (i < amount) {  // Ended moving
          let cell = this.boardGame.getCellById(this.cellOn);
          if (bTrigger) {
            cell.onTriggerEnd = () => {
              this.moveEnded();
            };
            cell.trigger(this);
          }
          else {
            this.moveEnded();
          }
          clearInterval(moveInterval);
        }
        else {  // Move by 1
          sfx.move.play();
          this.boardGame.playerMovingI = amount - i;
          let prevCells = this.boardGame.getPrevCells(this.cellOn);
          // if (prevCells.length == 1) {  // Move by 1
          //   this.cellOn = prevCells[0];
          //   let cell = this.boardGame.getCellById(this.cellOn);
          //   if (cell.bForceTrigger) {  // Force trigger
          //     cell.onTriggerEnd = () => {this.move(amount - i, bTrigger)};
          //     cell.trigger(this);
          //     clearInterval(moveInterval);
          //   }
          // }
          // else {  // Prev cell can be multiple. Finish moving
          //   let cell = this.boardGame.getCellById(this.cellOn);
          //   if (bTrigger) {
          //     cell.onTriggerEnd = () => {
          //       this.boardGame.bDrawPlayerMoving = false;
          //       this.boardGame.next()
          //     };
          //     cell.trigger(this);
          //   }
          //   else {
          //     this.boardGame.bDrawPlayerMoving = false;
          //     this.boardGame.next();
          //   }
          //   clearInterval(moveInterval);
          // }
          this.cellOn = random(prevCells);
          let cell = this.boardGame.getCellById(this.cellOn);
          if (cell.bForceTrigger) {  // Force trigger
            cell.onTriggerEnd = () => {this.move(amount - i, bTrigger)};
            cell.trigger(this);
            clearInterval(moveInterval);
          }
        }
      }, 500);
    }
  }
  
  moveEnded() {
    let bFound = false;
    if (!this.bHasDiamond) {
      for (let player of this.boardGame.getCellById(this.cellOn).onPlayers) {
        if (player.bHasDiamond && player != this) {
          bFound = true;
          setTimeout(() => {
            newDice.onRollEnd = () => {
              if (newDice.result == "Steal") {
                player.bHasDiamond = false;
                this.bHasDiamond = true;
              }
              setTimeout(() => {
                this.boardGame.bDrawPlayerMoving = false;
                this.boardGame.next();
              }, 700);
            };
            newDice.roll(this, ["Steal", "Fail"]);
          }, 700);
          break;
        }
      }
    }
    if (!bFound) {
      this.boardGame.bDrawPlayerMoving = false;
      this.boardGame.next();
    }
  }
  
  draw() {
    // draw in BoardCell
  }
}