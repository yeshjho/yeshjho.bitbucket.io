// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class Border {
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  get tl() {
    return {x: this.x - this.w/2, y: this.y - this.h/2};
  }
  
  get br() {
    return {x: this.x + this.w/2, y: this.y + this.h/2};
  }
  
  get minx() {
    return this.x - this.w/2;
  }
  
  get maxx() {
    return this.x + this.w/2;
  }
  
  get miny() {
    return this.y - this.h/2;
  }
  
  get maxy() {
    return this.y + this.h/2;
  }
  
  get center() {
    return [this.x, this.y];
  }
}