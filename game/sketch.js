// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


let bIsLooping = true;
let sfx, img;

let EMiniGameBorder;

let paddleBallLevels = [];
let board;

let boardGame;

let dice, newDice, tieBreakerDice, ui;

let tempPlayers = [];
let buttons = [];

function preload() {
  sfx = {
    ballBounce: loadSound("sfx/ball_bounce.wav"),
    warp: loadSound("sfx/warp.wav"),
    move: loadSound("sfx/move.wav"),
    roll: loadSound("sfx/roll.wav"),
    tada: loadSound("sfx/tada.wav"),
    diamond_bgm: loadSound("sfx/bgm1.wav"),
    orbital_bgm: loadSound("sfx/space_bgm.wav"),
    explosion: loadSound("sfx/explosion.wav"),
    coin_up: loadSound("sfx/coin3.wav"),
    coin_down: loadSound("sfx/coin-3.wav"),
    ship_hit: loadSound("sfx/hit.wav"),
  }
  sfx.warp.setVolume(0.5);
  sfx.tada.setVolume(0.4);
  sfx.coin_up.setVolume(0.5);
  sfx.coin_down.setVolume(0.5);
  sfx.diamond_bgm.setVolume(0.5);
  sfx.diamond_bgm.playMode("untilDone");
  sfx.orbital_bgm.setVolume(0.5);
  sfx.explosion.setVolume(0.4);
  sfx.ship_hit.setVolume(0.5);
  
  img = {
    cell_hidden: loadImage("img/hidden.png"),
    cell_4: loadImage("img/ufo.png"),
    warp_hell: loadImage("img/warp_hell.png"),
    warp_heaven: loadImage("img/diamond.png"),
    warp_overworld: loadImage("img/warp_overworld.png"),
    warp_1: loadImage("img/warp_circle.png"),
    warp_2: loadImage("img/warp_triangle.png"),
    warp_3: loadImage("img/warp_square.png"),
    warp_4: loadImage("img/warp_star.png"),
    cell_6: loadImage("img/question_marks.png"),
    cell_7: loadImage("img/fight.png"),
    shining_diamond: loadImage("img/shining_diamond.png"),
    cell_9: loadImage("img/four_arrows.png"),
    cell_10: loadImage("img/shop.png"),
    cell_11: loadImage("img/two_vs_two.png"),
    cell_12: loadImage("img/one_vs_three.png"),
    
    coin: loadImage("img/coin.png"),
    
    item_UP: loadImage("img/up.png"),
    item_DOWN: loadImage("img/down.png"),
    item_LEFT: loadImage("img/left.png"),
    item_RIGHT: loadImage("img/right.png"),
    item_ITEM: loadImage("img/item_item.png"),
    item_DICE: loadImage("img/item_dice.png"),
    
    "item_-1": loadImage("img/ui_quit.png"),
    item_0: loadImage("img/shining_diamond.png"),
    item_1: loadImage("img/thief.png"),
    item_2: loadImage("img/plus3.png"),
    item_3: loadImage("img/minus3.png"),
    item_4: loadImage("img/mul2.png"),
    item_5: loadImage("img/dice_odd.png"),
    item_6: loadImage("img/dice_even.png"),
    
    wasd: loadImage("img/wasd.png"),
    ijkl: loadImage("img/ijkl.png"),
    arrows: loadImage("img/arrows.png"),
    numpad: loadImage("img/numpad.png"),
  }
  
  for (let i = 2; i < 5; ++i) {
    paddleBallLevels.push(loadJSON("minigames/paddleball/lvl" + i + ".json"));
  }
  
  board = loadJSON("board1.json");
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  EMiniGameBorder = {
    0: new Border(width/2, height/2, width, height),
    1: [new Border(width/4, height/2, width/2, height),
        new Border(3*width/4, height/2, width/2, height)],
    2: [new Border(width/2, height/4, width, height/2),
        new Border(width/2, 3*height/4, width, height/2)],
    3: [new Border(width/2, height/4, width, height/2),
        new Border(width/4, 3*height/4, width/2, height/2),
        new Border(3*width/4, 3*height/4, width/2, height/2)],
    4: [new Border(width/4, height/4, width/2, height/2),
        new Border(3*width/4, height/4, width/2, height/2),
        new Border(width/4, 3*height/4, width/2, height/2),
        new Border(3*width/4, 3*height/4, width/2, height/2)]
  }
  
  dice = new Dice();
  newDice = new Dice();
  tieBreakerDice = new TieBreakerDice();
  ui = new UI();
  
  let plyerCount;
  do {
    playerCount = int(prompt("How many players are there? (2-4)"));
  }
  while (playerCount != 2 && playerCount != 3 && playerCount != 4);
  boardGame = new BoardGame(playerCount);
  
  for (let i = 0; i < playerCount; ++i) {
    tempPlayers.push({
      name: "Click Here to Enter Name",
      color: "Click Here to Choose Color",
      control: "Click Here to Choose Control",
    });
  }
  
  let playerSlotWidth = width / playerCount;
  for (let i = 0; i < tempPlayers.length; ++i) {
    let x = playerSlotWidth / 2 * (2 * i + 1);
    buttons.push(new Button(new Border(x, height/4, playerSlotWidth, 100), i,
                            "Enter the name:", "name"));
    buttons.push(new Button(new Border(x, height/2, playerSlotWidth, 100), i,
                            "Enter the color:\nCSS colors are available", "color"));
    buttons.push(new Button(new Border(x, height*3/4, playerSlotWidth, 100), i,
                            "Enter control:\nw: WASD / i: IJKL / 0: Arrows / 8: Numpad",
                            "control"));
  }
  buttons.push(new Button(new Border(width/2, height*17/18, width, height/9),
                          NaN, "", "confirm"));
}

function draw() {
  background(255);
  boardGame.update();
  boardGame.draw();
}

function keyPressed() {
  if (!bIsLooping)
    return;
  if (boardGame.miniGame)
    boardGame.miniGame.onKeyPressed(keyCode);
  switch(keyCode) {
    case 32:  // Spacebar
      if (boardGame.state == EGameState.GameEnd)
        window.location.reload();
      break;
    case 89:  // Y  // Y/N will work during explanation
      if (boardGame.state == EGameState.Explanation)
        boardGame.nextExplanation();
      break;
    case 78:  // N
      if (boardGame.state == EGameState.Explanation)
        boardGame.next();
      break;
    case W:
      tieBreakerDice.stop(EControl.WASD);
      if (dice.bRolling && dice.player.control == EControl.WASD) {
        dice.stop();
      }
      else if (newDice.bRolling && newDice.player.control == EControl.WASD) {
        newDice.stop();
      }
      else if (ui.bOpened && ui.player.control == EControl.WASD) {
        ui.select();
      }
      break;
    case A:
      if (ui.bOpened && ui.player.control == EControl.WASD) {
        ui.showPrevItem();
      }
      break;
    case S:
      break;
    case D:
      if (ui.bOpened && ui.player.control == EControl.WASD) {
        ui.showNextItem();
      }
      break;
      
    case I:
      tieBreakerDice.stop(EControl.IJKL);
      if (dice.bRolling && dice.player.control == EControl.IJKL) {
        dice.stop();
      }
      else if (newDice.bRolling && newDice.player.control == EControl.IJKL) {
        newDice.stop();
      }
      else if (ui.bOpened && ui.player.control == EControl.IJKL) {
        ui.select();
      }
      break;
    case J:
      if (ui.bOpened && ui.player.control == EControl.IJKL) {
        ui.showPrevItem();
      }
      break;
    case K:
      break;
    case L:
      if (ui.bOpened && ui.player.control == EControl.IJKL) {
        ui.showNextItem();
      }
      break;
      
    case AUP:
      tieBreakerDice.stop(EControl.Arrows);
      if (dice.bRolling && dice.player.control == EControl.Arrows) {
        dice.stop();
      }
      else if (newDice.bRolling && newDice.player.control == EControl.Arrows) {
        newDice.stop();
      }
      else if (ui.bOpened && ui.player.control == EControl.Arrows) {
        ui.select();
      }
      break;
    case ALEFT:
      if (ui.bOpened && ui.player.control == EControl.Arrows) {
        ui.showPrevItem();
      }
      break;
    case ADOWN:
      break;
    case ARIGHT:
      if (ui.bOpened && ui.player.control == EControl.Arrows) {
        ui.showNextItem();
      }
      break;
      
    case NUM8:
      tieBreakerDice.stop(EControl.Numpad);
      if (dice.bRolling && dice.player.control == EControl.Numpad) {
        dice.stop();
      }
      else if (newDice.bRolling && newDice.player.control == EControl.Numpad) {
        newDice.stop();
      }
      else if (ui.bOpened && ui.player.control == EControl.Numpad) {
        ui.select();
      }
      break;
    case NUM4:
      if (ui.bOpened && ui.player.control == EControl.Numpad) {
        ui.showPrevItem();
      }
      break;
    case NUM5:
      break;
    case NUM6:
      if (ui.bOpened && ui.player.control == EControl.Numpad) {
        ui.showNextItem();
      }
      break;
  }
}

function mouseClicked() {
  switch (boardGame.state) {
    case EGameState.Intro:
      buttons.forEach(button => button.checkAndActivate(mouseX, mouseY));
      break;
    case EGameState.Explanation:  // Clicking will work while selecting Y/N
      boardGame.nextExplanation();
      break;
  }
}