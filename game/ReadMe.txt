All image and sound files are self-created.

[Image]
All files are drawn using https://pixlr.com/x/

[Sound]
All files are modified using GoldWave
- Recorded using GoldWave
    ball_bounce.wav
    warp.wav
	roll.wav
	move.wav
- Made using LMMS
    tada.wav
	bgm1.wav
	space_bgm.wav
- Made using BFXR
    explosion.wav
	coin3.wav
	coin-3.wav
	hit.wav