// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class UI {
  constructor() {
    this.items = [];  // Array[Array[EItemType, cost]]
    this.player = null;
    
    this._currentIndex = 0;
    
    this.bOpened = false;
    this.bIsBuying = true;
    
    this.onUIEnd = null;
    this.onUIEndArguments = [];
  }
  
  open(items, player, bNoQuit=false, bIsBuying=true) {
    this._currentIndex = 0;
    this.items = [...items];
    if (!bNoQuit)
      this.items.push([EItemType.Quit, 0]);
    this.player = player;
    this.bOpened = true;
    this.bIsBuying = bIsBuying;
  }
  
  get currentIndex() {
    return this._currentIndex;
  }
  
  set currentIndex(value) {
    if (value < 0) {
      value = this.items.length - 1;
    }
    value %= this.items.length;
    
    this._currentIndex = value;
  }
  
  showPrevItem() {
    this.currentIndex--;
  }
  
  showNextItem() {
    this.currentIndex++;
  }
  
  select() {
    if (!this.bOpened)
      return;
      
    let item = this.items[this.currentIndex];
    if (item[0] == EItemType.Quit) {
      this.quit();
    }
    
    else if (this.player.coin >= item[1]) {
      switch (item[0]) {
        case EItemType.Diamond:
          this.player.bHasDiamond = true;
          this.player.boardGame.getCellById(this.player.cellOn).bHasDiamond = false;
          break;
        case EItemType.Up:
        case EItemType.Down:
        case EItemType.Left:
        case EItemType.Right:
        case EItemType.Item:
        case EItemType.Dice:
          this.onUIEndArguments.push(item[0]);
          this.quit();
          break;
        default:
          if (this.bIsBuying)
            this.player.items.push(item[0]);
          else {
            this.onUIEndArguments.push(item[0]);
            this.quit();
          }
          break;
      }
      
      this.player.coin -= item[1];
      this.items.splice(this.currentIndex, 1);
      
      if (this.items.length == 1 && this.items[0][0] == EItemType.Quit) {
        this.quit();
      }
      this.currentIndex--;
    }
  }
  
  draw() {
    if (this.bOpened) {
      let curIndex = this.currentIndex;
      let prevIndex = curIndex == 0 ? this.items.length - 1 : curIndex - 1;
      let nextIndex = curIndex == this.items.length - 1 ? 0 : curIndex + 1;
      let coinImageX;
      let item;
      push();
      fill(0);
      stroke(255);
      strokeWeight(2);
      textSize(20);
      textAlign(CENTER, CENTER);
      text("UP: Select\nLEFT/RIGHT: Rotate", width/2, height/4);
      
      imageMode(CENTER);
      rectMode(CENTER);
      
      item = this.items[prevIndex];
      fill(255, 100);
      rect(width/4, height/2, 150, 150);
      if (img["item_" + item[0]])
        image(img["item_" + item[0]], width/4, height/2, 150, 150);
      else
        text(item[0], width/4, height/2, 150, 150);
      
      noFill();
      strokeWeight(3);
      stroke(this.player.color);
      rect(width/4, height/2, 150, 150);
      
      fill(0);
      textAlign(CENTER, TOP);
      textSize(20);
      stroke(255);
      strokeWeight(10);
      text(item[1], width/4, height/2 + 150/2 + 10);
      
      coinImageX = width/4 - textWidth(item[1]) - 17;
      image(img.coin, coinImageX, height/2 + 150/2 + 20, 30, 30);
      
      
      item = this.items[curIndex];
      fill(255, 180);
      rect(width/2, height/2, 200, 200);
      if (img["item_" + item[0]])
        image(img["item_" + item[0]], width/2, height/2, 200, 200);
      else
        text(item[0], width/2, height/2, 200, 200);
      
      noFill();
      strokeWeight(3);
      stroke(this.player.color);
      rect(width/2, height/2, 200, 200);
      
      fill(0);
      textAlign(CENTER, TOP);
      textSize(25);
      stroke(255);
      strokeWeight(10);
      text(item[1], width/2, height/2 + 200/2 + 10);
      
      text(EItemDescription[item[0]], width/2, height/2 + 200/2 + 50);
      
      coinImageX = width/2 - textWidth(item[1]) - 17;
      image(img.coin, coinImageX, height/2 + 200/2 + 20, 30, 30);
      
      
      item = this.items[nextIndex]
      fill(255, 100);
      rect(width*3/4, height/2, 150, 150);
      if (img["item_" + item[0]])
        image(img["item_" + item[0]], width*3/4, height/2, 150, 150);
      else
        text(item[0], width*3/4, height/2, 150, 150);
      
      noFill();
      strokeWeight(3);
      stroke(this.player.color);
      rect(width*3/4, height/2, 150, 150);
      
      fill(0);
      textAlign(CENTER, TOP);
      textSize(20);
      stroke(255);
      strokeWeight(10);
      text(item[1], width*3/4, height/2 + 150/2 + 10);
      
      coinImageX = width*3/4 - textWidth(item[1]) - 17;
      image(img.coin, coinImageX, height/2 + 150/2 + 20, 30, 30);
      
      pop();
    }
  }
  
  quit() {
    this.bOpened = false;
    if (this.onUIEnd) {
      this.onUIEnd(...this.onUIEndArguments);
      this.onUIEnd = null;
      this.onUIEndArguments = [];
    }
  }
}