// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class Dice {
  constructor() {
    this.result = null;
    this.player = null;
    this.items = [];
    
    this.textIntervals = [];
    
    this.bDraw = false;
    this.bRolling = false;
    
    this.notIn = [];
    
    this.onRollEnd = null;
  }
  
  roll(player, items=[1, 2, 3, 4, 5, 6]) {
    this.player = player;
    this.items = [...items];
    sfx.roll.loop();
    
    let i = floor(random(items.length));
    this.result = items[i];
    this.bRolling = true;
    this.bDraw = true;
    this.textIntervals.push(setInterval(() => {
      this.result = items[i % this.items.length];
      i++;
    }, 10));
  }
  
  stop(bStay=false) {
    if (!this.bRolling)
      return;
    this.bRolling = false;
    this.textIntervals.forEach(interval => clearInterval(interval));
    sfx.roll.stop();
    sfx.tada.play();
    do {
      this.result = random(this.items);
    }
    while (this.notIn.indexOf(this.result) != -1);
    this.notIn = [];
    setTimeout(() => {
      if (!bStay)
        this.bDraw = false;
      if (this.onRollEnd) {
        this.onRollEnd();
        this.onRollEnd = null;
      }
    }, 1000);
  }
  
  draw() {
    if (this.bDraw) {
      push();
      fill(0);
      stroke(255);
      strokeWeight(2);
      textSize(20);
      textAlign(CENTER, CENTER);
      text("Press Up to roll the dice.", width/2, height/4);
      
      rectMode(CENTER);
      strokeWeight(5);
      stroke(this.player.color);
      fill(255, 210);
      rect(width/2, height/2, 200, 200);
      fill(0);
      strokeWeight(2);
      textSize(50);
      textAlign(CENTER, CENTER);
      stroke(255);
      text(this.result, width/2, height/2);
      pop();
    }
  }
}