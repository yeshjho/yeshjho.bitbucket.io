// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


class TieBreakerDice {
  constructor() {
    this.dices = [];
    this.results = [];
    
    this.originalRank = [];
    this.resultRank = [];
    
    this.onRollEnd = null;
    
    this.bDraw = false;
  }
  
  roll(rank) {
    this.originalRank = [...rank];
    this.resultRank = [];
    this.dices = [{}, {}];
    this.results = [{}, {}];
    
    this.bDraw = true;
    
    let slotI = 0;
    let playerI = 1;
    for (let place of rank) {
      if (place.length >= 2) {
        for (let player of place) {
          let dice = new Dice();
          let widthOffset = playerI;
          let diceColor = slotI ? [255, 255, 100] : [255, 255, 255];
          dice.draw = () => {
            if (dice.bDraw) {
              push();
              rectMode(CENTER);
              strokeWeight(5);
              stroke(player.color);
              fill(...diceColor, 210);
              rect(width*widthOffset/5, height/2, 100, 100);
              fill(0);
              strokeWeight(2);
              textSize(30);
              textAlign(CENTER, CENTER);
              stroke(255);
              text(dice.result, width*widthOffset/5, height/2);
              pop();
            }
          };
          
          this.dices[slotI][player.control] = dice;
          dice.roll(player, [1, 2, 3, 4, 5, 6]);
          
          playerI++;
        }
        slotI++;
      }
    }
    if (Object.values(this.dices[0]).length == 0) {
      this.checkEnd();
    }
  }
  
  stop(control) {
    if (!this.bDraw)
      return;
      
    for (let playerControl in this.dices[0]) {
      if (playerControl == control) {
        let dice = this.dices[0][playerControl];
        if (!dice.bRolling)
          continue;
          
        dice.notIn = Object.values(this.results[0]);
        dice.stop(true);
        this.results[0][playerControl] = dice.result;
        this.checkEnd();
      }
    }
    for (let playerControl in this.dices[1]) {
      if (playerControl == control) {
        let dice = this.dices[1][playerControl];
        if (!dice.bRolling)
          continue;
          
        dice.notIn = Object.values(this.results[1]);
        dice.stop(true);
        this.results[1][playerControl] = dice.result;
        this.checkEnd();
      }
    }
  }
  
  checkEnd() {
    if (Object.values(this.dices[0]).length + Object.values(this.dices[1]).length ==
        Object.values(this.results[0]).length + Object.values(this.results[1]).length) {
      
      let slotI = 0;
      for (let place of this.originalRank) {
        switch (place.length) {
          case 0:
            continue;
          case 1:
            this.resultRank.push(place[0]);
            break;
          default:
            let rank =
              Object.entries(this.results[slotI]).sort((p1, p2) => {return p2[1] - p1[1]});
            for (let [playerControl, _] of rank) {
              for (let player of boardGame.players) {
                if (player.control == playerControl) {
                  this.resultRank.push(player);
                  break;
                }
              }
            }
            slotI++;
            break;
        }
      }
      
      setTimeout(() => {
        this.bDraw = false;
        if (this.onRollEnd) {
          this.onRollEnd();
          this.onRollEnd = null;
        }
      }, 1000);
    }
  }
  
  draw() {
    if (this.bDraw) {
      push();
      fill(0);
      textSize(20);
      textAlign(CENTER, CENTER);
      text("Press Up to roll the dice.", width/2, height/4);
      pop();
      
      [...Object.values(this.dices[0]), ...Object.values(this.dices[1])].forEach(
        dice => dice.draw());
    }
  }
}