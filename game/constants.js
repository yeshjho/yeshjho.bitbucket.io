// Written by Joonho Hwang
// Final Project
// CS099
// Spring 2019


const W = 87;
const A = 65;
const S = 83;
const D = 68;

const AUP = 38;
const ALEFT = 37;
const ADOWN = 40;
const ARIGHT = 39;

const I = 73;
const J = 74;
const K = 75;
const L = 76;

const NUM8 = 104;
const NUM4 = 100;
const NUM5 = 101;
const NUM6 = 102;

const HIGHLIGHT_COLOR = [0, 226, 124];

const MINIGAME_TURN_GAP = 5;
const THIEF_STEAL_TURNS = 3;

const EControl = {
  WASD: 0,
  Arrows: 1,
  IJKL: 2,
  Numpad: 3,
}

const EItemType = {
  Up: "UP",
  Down: "DOWN",
  Left: "LEFT",
  Right: "RIGHT",
  
  Item: "ITEM",
  Dice: "DICE",
  
  Quit: -1,
  
  Diamond: 0,
  
  Thief: 1,
  Add3: 2,
  Minus3: 3,
  Mul2: 4,
  OddOnly: 5,
  EvenOnly: 6,
}

const EItemCost = {
  0: 100,
  1: 200,
  2: 20,
  3: 15,
  4: 50,
  5: 25,
  6: 25
}

const EItemDescription = {
  "-1": "Quit",
  
  0: "Ooh, Shiny!",
  
  1: "Hire a thief and steal the diamond! Takes " + THIEF_STEAL_TURNS +
    " turns to steal.\nThere can be only 1 active thief.",
  2: "Add 3 to your dice result.",
  3: "Subtract 3 from your dice result.",
  4: "Multiply your dice result by 2.",
  5: "Make your dice result odd numbers only.",
  6: "Make your dice result even numbers only."
}

const EGameState = {
  Intro: -1,
  BoardGame: 0,
  MiniGame: 1,
  TieBreaker: 2,
  MiniGameResult: 3,
  GameEnd: 4,
  Explanation: 5,
}

const ECellType = {
  StartingPoint: -1,
  CoinPlus: 0,
  CoinMinus: 1,
  MoveForward: 2,
  MoveBackward: 3,
  UFO: 4,
  Warp: 5,
  ResetRandomly: 6,
  OneOnOneFight: 7,
  Diamond: 8,
  Seperator: 9,
  Shop: 10,
  TwoOnTwoFight: 11,
  OneOnThreeFight: 12,
}

const ECellColor = {
  "-1": [157, 127, 97],
  0: [100, 100, 255],
  1: [255, 100, 100],
  2: [100, 255, 100],
  3: [255, 255, 100],
  4: [255, 205, 18],
  5: [255, 100, 255],
  6: [0, 255, 255],
  7: [200, 50, 50],
  8: [200, 255, 255],
  9: [255, 255, 255],
  10: [249, 166, 2],
  11: [200, 50, 50],
  12: [200, 50, 50],
}

const EMiniGameType = {
  Individual: 0,
  TwoOnTwo: 1,
  OneOnThree: 2
}

const EMiniGameTitle = {
  0: {
    PaddleBall: 0,
    Orbital: 1,
    // PassBomb: 2,
  },
  
  1: {
    TwoOnTwoShooting: 100,
  },
  
  2: {
    JumpRope: 1000,
  }
}

const EMiniGameWindow = {
  One: 0,
  TwoVertical: 1,
  TwoHorizontal: 2,
  Three: 3,
  Four: 4
}

const MINIGAME_PRIZE = {
    2: [8, 3],
    3: [10, 6, 2],
    4: [14, 9, 4, 2]
}

const EOrdinal = {
  1: "1st",
  2: "2nd",
  3: "3rd",
  4: "4th"
}

const FRAME_ERROR = 5;