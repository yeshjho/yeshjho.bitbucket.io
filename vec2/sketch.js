// Written by Joonho Hwang
// Create a Vector2 Class Assignment
// CS099
// Spring 2019


function toDegrees(radians) {
  return radians / PI * 180;
}

class Vec2 {
  constructor(x=0, y=0) {
    this.x = x;
    this.y = y;
    this.r = random(30, 60);
  }
  
  getAngle() {
    return atan2(this.y, this.x);
  }
  
  getDegreeAngle() {
    let angle = this.getAngle();
    return angle >= 0 ? toDegrees(angle) : toDegrees(angle + 2*PI);
  }
  
  setAngle(angle_in_radians) {
    let length = this.getLength()
    this.x = length * cos(angle_in_radians);
    this.y = length * sin(angle_in_radians);
  }
  
  getLength() {
    return sqrt(this.x**2 + this.y**2);
  }
  
  setLength(length) {
    let angle = this.getAngle()
    this.x = length * cos(angle);
    this.y = length * sin(angle);
  }
  
  add(v2) {
    return new Vec2(this.x + v2.x, this.y + v2.y);
  }
  
  addTo(v2) {
    this.x += v2.x;
    this.y += v2.y;
  }
  
  subtract(v2) {
    return new Vec2(this.x - v2.x, this.y - v2.y);
  }
  
  subtractFrom(v2) {
    this.x -= v2.x;
    this.y -= v2.y;
  }
  
  multiply(scalar) {
    return new Vec2(this.x * scalar, this.y * scalar);
  }
  
  multiplyBy(scalar) {
    this.x *= scalar;
    this.y *= scalar;
  }
  
  divide(scalar) {
    return new Vec2(this.x / scalar, this.y / scalar);
  }
  
  divideBy(scalar) {
    this.x /= scalar;
    this.y /= scalar;
  }
  
  draw(starting_point_vector=new Vec2(0, 0)) {
    /*line(origin_x + starting_point_vector.x,
         origin_y - starting_point_vector.y,
         origin_x + this.x + starting_point_vector.x,
         map(this.y, 0, origin_y, origin_y, 0) - starting_point_vector.y);*/
    push();
    translate(origin_x + this.x + starting_point_vector.x,
              map(this.y, 0, origin_y, origin_y, 0) - starting_point_vector.y);
    rotate(map(this.getAngle(), 0, 2*PI, 2*PI, 0));
    beginShape();
    vertex(0, 0);
    vertex(-10, -10);
    vertex(0, 0);
    vertex(-10, 10);
    vertex(0, 0);
    vertex(-this.getLength(), 0);
    endShape();
    pop();
  }
  
  drawText(_text, starting_point_vector=new Vec2(0, 0)) {
    text(_text,
         origin_x + this.x/2 + starting_point_vector.x,
         map(this.y/2, 0, origin_y, origin_y, 0) - starting_point_vector.y);
  }
  
  drawInfo(_text, starting_point_vector=new Vec2(0, 0)) {
    text(_text + "\n" + round(this.getLength()) + "u\n" + round(this.getDegreeAngle()) + "º",
         origin_x + this.x/2 + starting_point_vector.x,
         map(this.y/2, 0, origin_y, origin_y, 0) - starting_point_vector.y);
  }
}

let origin_x, origin_y;
let one_s, v1, sv1, v2, va;
let two_s, v3, sv3, v4, vb;
let mouseVector;

function setupVectors() {
  origin_x = width/2;
  origin_y = height/2;
  
  // 1.
  one_s = 2;
  v1 = new Vec2(80*width/400, 30*height/400);
  sv1 = v1.multiply(one_s);
  v2 = new Vec2(-100*width/400, -170*height/400);
  va = sv1.add(v2);
  
  // 2.
  two_s = 3;
  v3 = new Vec2(-150*width/400, 150*height/400);
  sv3 = v3.divide(two_s);
  v4 = new Vec2(-150*width/400, -100*height/400);
  vb = sv3.subtract(v4);
  
  // 3.
  mouseVector = new Vec2();
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  setupVectors();
}

function draw() {
  background(220);
  stroke(0);
  strokeWeight(1);
  textSize(12);
  
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);
  
  strokeWeight(2);
  
  // 1.
  stroke(255, 0, 0);
  v1.draw();
  stroke(255, 150, 150);
  sv1.draw();
  stroke(0, 0, 255);
  v2.draw(sv1);
  stroke(0, 255, 0);
  va.draw();
  
  push();
  noStroke(0);
  v1.drawInfo("A");
  sv1.drawText("sA");
  v2.drawInfo("B", sv1);
  va.drawInfo("R");
  text("sA + B = R\ns=" + one_s, width*3/4, height*3/4);
  pop();
  
  // 2.
  stroke(255, 0, 0);
  v3.draw();
  stroke(255, 150, 150);
  sv3.draw();
  stroke(0, 0, 255);
  v4.draw();
  stroke(0, 255, 0);
  vb.draw(v4);
  
  push();
  noStroke(0);
  v3.drawInfo("A'");
  sv3.drawText("1/sA'");
  v4.drawInfo("B'");
  vb.drawInfo("R'", v4);
  text("1/sA' - B' = R'\ns=" + two_s, width/6, height/6);
  pop();
  
  // 3.
  mouseVector.setLength(map(mouseY, 0, height, 0, sqrt((width/2)**2 + (height/2)**2), true));
  mouseVector.setAngle(map(mouseX, 0, width, 0, 2*PI, true));
  stroke(0, 255, 255);
  mouseVector.draw();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  setupVectors();
}